﻿using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace iOrder.Data
{
    public class OrderDbContext : DbContext, IOrderDbContext
    {
        public OrderDbContext(DbContextOptions<OrderDbContext> options)
            : base(options)
        {
            Database.Migrate();
        }

        public DbSet<Basket> Baskets { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<ContractType> ContractTypes { get; set; }
        public DbSet<Factory> Factories { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<OrderItemState> OrderItemStates { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<OrganizationPriceSetting> OrganizationPriceSettings { get; set; }
        public DbSet<OrganizationPromotion> OrganizationPromotions { get; set; }
        public DbSet<OrganizationSetting> OrganizationSettings { get; set; }
        public DbSet<OrganizationType> OrganizationTypes { get; set; }
        public DbSet<Point> Points { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductPrice> ProductPrices { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<Promotion> Promotions { get; set; }
        public DbSet<Provision> Provisions { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserPoint> UserPoints { get; set; }
        public DbSet<UserSetting> UserSettings { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(OrderDbContext).Assembly);

            #region Production

            modelBuilder.Entity<ProductType>().HasData(new ProductType() { Id = 1, Type = "Home" });
            modelBuilder.Entity<ProductType>().HasData(new ProductType() { Id = 2, Type = "Women" });
            modelBuilder.Entity<ProductType>().HasData(new ProductType() { Id = 3, Type = "Men" });
            modelBuilder.Entity<ProductType>().HasData(new ProductType() { Id = 5, Type = "Shoes" });
            modelBuilder.Entity<ProductType>().HasData(new ProductType() { Id = 6, Type = "Jewelry" });
            modelBuilder.Entity<ProductType>().HasData(new ProductType() { Id = 7, Type = "Accessories" });

            modelBuilder.Entity<OrganizationType>().HasData(new OrganizationType() { Id = 1, Deleted = false, Type = "Vip" });
            modelBuilder.Entity<OrganizationType>().HasData(new OrganizationType() { Id = 2, Deleted = false, Type = "Partner" });
            modelBuilder.Entity<OrganizationType>().HasData(new OrganizationType() { Id = 3, Deleted = false, Type = "Other" });

            modelBuilder.Entity<ContractType>().HasData(new ContractType() { Id = 1, Deleted = false, Type = "Full" });
            modelBuilder.Entity<ContractType>().HasData(new ContractType() { Id = 2, Deleted = false, Type = "Half" });

            modelBuilder.Entity<OrderItemState>().HasData(new OrderItemState() { Id = 1, Deleted = false, State = "Принят" });
            modelBuilder.Entity<OrderItemState>().HasData(new OrderItemState() { Id = 2, Deleted = false, State = "Обработка" });
            modelBuilder.Entity<OrderItemState>().HasData(new OrderItemState() { Id = 3, Deleted = false, State = "В пути" });
            modelBuilder.Entity<OrderItemState>().HasData(new OrderItemState() { Id = 4, Deleted = false, State = "Доставлен" });
            modelBuilder.Entity<OrderItemState>().HasData(new OrderItemState() { Id = 5, Deleted = false, State = "Отменен" });

            modelBuilder.Entity<Role>().HasData(new Role() { Id = new Guid("7599C376-8548-453E-9F8A-87F18AC413AF"), Name = "Administrator" });
            modelBuilder.Entity<Role>().HasData(new Role() { Id = new Guid("89E30A9D-4B12-44A8-A346-61D4A34EEAB3"), Name = "Manager" });
            modelBuilder.Entity<Role>().HasData(new Role() { Id = new Guid("6A0AEC92-3C66-4D84-BA46-FD78C58F8020"), Name = "User" });

            modelBuilder.Entity<User>().HasData(new User() { Id = new Guid("67F6312B-CAB9-4F55-8B2D-52784A5BB1B4"), Email = "iorder@company.io", Login = "admin", Password = "admin", Name = "Administrator", Created = DateTime.MinValue, Updated = DateTime.MinValue });
            modelBuilder.Entity<UserSetting>().HasData(new User() { Id = new Guid("67F6312B-CAB9-4F55-8B2D-52784A5BB1B4"), Created = DateTime.MinValue, Updated = DateTime.MinValue });

            modelBuilder.Entity<UserRole>().HasData(new UserRole() { UserId = new Guid("67F6312B-CAB9-4F55-8B2D-52784A5BB1B4"), RoleId = new Guid("7599C376-8548-453E-9F8A-87F18AC413AF") });

            #endregion
        }
    }
}
