﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class OrderItemConfiguration : IEntityTypeConfiguration<OrderItem>
    {
        public void Configure(EntityTypeBuilder<OrderItem> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.Lot);

            builder.Property(e => e.Expiration).IsRequired(false);

            builder.Property(e => e.Price).IsRequired();

            builder.Property(e => e.Quantity).IsRequired();

            builder.Property(e => e.Updated).IsRequired();

            builder.Property(e => e.Created).IsRequired();

            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.OrderId);

            builder.HasIndex(e => e.ProductId);

            builder.HasIndex(e => e.OrderItemStateId);

            builder.ToTable("OrderItems");

            builder.HasOne(e => e.Order)
                .WithMany(e => e.OrderItems)
                .HasForeignKey(e => e.OrderId)
                .HasConstraintName("FK_OrderItems_Order");

            builder.HasOne(e => e.Product)
                .WithMany(e => e.OrderItems)
                .HasForeignKey(e => e.ProductId)
                .HasConstraintName("FK_OrderItems_Product");

            builder.HasOne(e => e.OrderItemState)
                .WithMany(e => e.OrderItems)
                .HasForeignKey(e => e.OrderItemStateId)
                .HasConstraintName("FK_OrderItems_OrderItemState")
                .IsRequired(false);
        }
    }
}
