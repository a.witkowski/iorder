﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class OrganizationPromotionConfiguration : IEntityTypeConfiguration<OrganizationPromotion>
    {
        public void Configure(EntityTypeBuilder<OrganizationPromotion> builder)
        {
            builder.Property(e => e.OrganizationId).IsRequired().IsConcurrencyToken();

            builder.Property(e => e.PromotionId).IsRequired().IsConcurrencyToken();

            builder.ToTable("OrganizationPromotions");

            builder.HasKey(e => new { e.OrganizationId, e.PromotionId });

            builder.HasOne(e => e.Organization)
                .WithMany(e => e.OrganizationPromotions)
                .HasForeignKey(e => e.OrganizationId);

            builder.HasOne(e => e.Promotion)
                .WithMany(e => e.OrganizationPromotions)
                .HasForeignKey(e => e.PromotionId);
        }
    }
}
