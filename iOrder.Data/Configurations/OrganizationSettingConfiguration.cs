﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class OrganizationSettingConfiguration : IEntityTypeConfiguration<OrganizationSetting>
    {
        public void Configure(EntityTypeBuilder<OrganizationSetting> builder)
        {
            builder.Property(e => e.Id);

            builder.Property(e => e.UsePriceSettings).HasDefaultValue(true);

            builder.Property(e => e.CreditDays).IsRequired().HasDefaultValue(0);

            builder.Property(e => e.UsePharmacy).HasDefaultValue(true);

            builder.Property(e => e.UseDietarySupplement).HasDefaultValue(true);

            builder.Property(e => e.UseOptics).HasDefaultValue(true);

            builder.Property(e => e.UseCosmetic).HasDefaultValue(true);

            builder.Property(e => e.UseMedicalEquipment).HasDefaultValue(true);

            builder.Property(e => e.UseMedicalProducts).HasDefaultValue(true);

            builder.Property(e => e.PharmacyManager);

            builder.Property(e => e.OpticsManager);

            builder.Property(e => e.CosmeticManager);

            builder.Property(e => e.MedicalEquipmentManager);

            builder.Property(e => e.PharmacyManagerPhone);

            builder.Property(e => e.OpticsManagerPhone);

            builder.Property(e => e.CosmeticManagerPhone);

            builder.Property(e => e.MedicalEquipmentManagerPhone);

            builder.Property(e => e.PharmacyManagerEmail);

            builder.Property(e => e.OpticsManagerEmail);

            builder.Property(e => e.CosmeticManagerEmail);

            builder.Property(e => e.MedicalEquipmentManagerEmail);

            builder.Property(e => e.PharmacyManagerSkype);

            builder.Property(e => e.OpticsManagerSkype);

            builder.Property(e => e.CosmeticManagerSkype);

            builder.Property(e => e.MedicalEquipmentManagerSkype);

            builder.Property(e => e.Updated).IsRequired();

            builder.Property(e => e.Created).IsRequired();

            builder.HasKey(e => e.Id);

            builder.ToTable("OrganizationSettings");
        }
    }
}
