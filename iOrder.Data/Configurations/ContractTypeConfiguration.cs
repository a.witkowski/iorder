﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class ContractTypeConfiguration : IEntityTypeConfiguration<ContractType>
    {
        public void Configure(EntityTypeBuilder<ContractType> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.Type).IsRequired();

            builder.Property(e => e.Deleted).IsRequired().HasDefaultValue(false);

            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.Type);

            builder.ToTable("ContractTypes");
        }
    }
}
