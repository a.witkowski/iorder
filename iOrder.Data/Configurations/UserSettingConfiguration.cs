﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class UserSettingConfiguration : IEntityTypeConfiguration<UserSetting>
    {
        public void Configure(EntityTypeBuilder<UserSetting> builder)
        {
            builder.Property(e => e.Id);

            builder.Property(e => e.UsePharmacy).HasDefaultValue(true);

            builder.Property(e => e.UseDietarySupplement).HasDefaultValue(true);

            builder.Property(e => e.UseOptics).HasDefaultValue(true);

            builder.Property(e => e.UseCosmetic).HasDefaultValue(true);

            builder.Property(e => e.UseMedicalEquipment).HasDefaultValue(true);

            builder.Property(e => e.UseMedicalProducts).HasDefaultValue(true);

            builder.Property(e => e.Updated).IsRequired();

            builder.Property(e => e.Created).IsRequired();

            builder.HasKey(e => e.Id);

            builder.ToTable("UserSettings");

            builder.HasOne(e => e.User)
                .WithOne(e => e.UserSetting)
                .HasForeignKey<User>(e => e.Id);
        }
    }
}
