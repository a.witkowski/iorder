﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.Email).IsRequired().IsConcurrencyToken();

            builder.Property(e => e.Login).IsRequired().IsConcurrencyToken();

            builder.Property(e => e.Password).IsRequired().IsConcurrencyToken();

            builder.Property(e => e.Name);

            builder.Property(e => e.ParentId);

            builder.Property(e => e.Deleted).IsRequired().HasDefaultValue(false);

            builder.Property(e => e.Updated).IsRequired();

            builder.Property(e => e.Created).IsRequired();

            builder.HasKey(e => e.Id);

            builder.HasAlternateKey(e => e.Login);

            builder.HasIndex(e => e.OrganizationId);

            builder.HasIndex(e => e.Email);

            builder.HasIndex(e => e.ParentId);

            builder.ToTable("Users");

            builder.HasOne(e => e.Organization)
                .WithMany(e => e.Users)
                .HasForeignKey(e => e.OrganizationId)
                .HasConstraintName("FK_Users_Organization");

            builder.HasMany(e => e.ChildUsers)
                .WithOne(e => e.ParentUser)
                .HasForeignKey(e => e.ParentId)
                .HasConstraintName("FK_Users_User");
        }
    }
}
