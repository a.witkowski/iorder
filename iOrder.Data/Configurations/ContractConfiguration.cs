﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class ContractConfiguration : IEntityTypeConfiguration<Contract>
    {
        public void Configure(EntityTypeBuilder<Contract> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.Id1C).IsRequired();

            builder.Property(e => e.Code).IsRequired();

            builder.Property(e => e.Name);

            builder.Property(e => e.Start).IsRequired();

            builder.Property(e => e.End).IsRequired(false);

            builder.Property(e => e.Deleted);

            builder.Property(e => e.Updated).IsRequired();

            builder.Property(e => e.Created).IsRequired();

            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.OrganizationId);

            builder.HasIndex(e => e.ContractTypeId);

            builder.ToTable("Contracts");

            builder.HasOne(e => e.Organization)
                .WithMany(e => e.Contracts)
                .HasForeignKey(e => e.OrganizationId)
                .HasConstraintName("FK_Contracts_Organization");

            builder.HasOne(e => e.ContractType)
                .WithMany(e => e.Contracts)
                .HasForeignKey(e => e.ContractTypeId)
                .HasConstraintName("FK_Contracts_ContractTypes");
        }
    }
}
