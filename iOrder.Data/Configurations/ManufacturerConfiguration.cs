﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class ManufacturerConfiguration : IEntityTypeConfiguration<Manufacturer>
    {
        public void Configure(EntityTypeBuilder<Manufacturer> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.Id1C).IsRequired();

            builder.Property(e => e.Code).IsRequired();

            builder.Property(e => e.Name).IsRequired();

            builder.Property(e => e.PriceGroup).IsRequired(false);

            builder.Property(e => e.HideWholesale).HasDefaultValue(false);

            builder.Property(e => e.HideRetail).HasDefaultValue(false);

            builder.Property(e => e.HideBarter).HasDefaultValue(false);

            builder.Property(e => e.Deleted).HasDefaultValue(false);

            builder.Property(e => e.Updated).IsRequired();

            builder.Property(e => e.Created).IsRequired();

            builder.HasKey(e => e.Id);

            builder.ToTable("Manufacturers");
        }
    }
}
