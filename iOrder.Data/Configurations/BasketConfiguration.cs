﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class BasketConfiguration : IEntityTypeConfiguration<Basket>
    {
        public void Configure(EntityTypeBuilder<Basket> builder)
        {
            builder.Property(e => e.Lot).IsRequired();

            builder.Property(e => e.Expiration).IsRequired(false);

            builder.Property(e => e.Price).IsRequired();

            builder.Property(e => e.Quantity).IsRequired();

            builder.Property(e => e.Created).IsRequired();

            builder.HasKey(e => new { e.UserId, e.PointId, e.ProductId });

            builder.HasIndex(e => e.UserId);

            builder.ToTable("Baskets");

            builder.HasOne(e => e.User)
                .WithMany(e => e.Baskets)
                .HasForeignKey(e => e.UserId)
                .HasConstraintName("FK_Basket_Users");

            builder.HasOne(e => e.Point)
                .WithMany(e => e.Baskets)
                .HasForeignKey(e => e.PointId)
                .HasConstraintName("FK_Basket_Points");

            builder.HasOne(e => e.Product)
                .WithMany(e => e.Baskets)
                .HasForeignKey(e => e.ProductId)
                .HasConstraintName("FK_Basket_Products");
        }
    }
}
