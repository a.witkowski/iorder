﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.Id1C).IsRequired();

            builder.Property(e => e.Code).IsRequired();

            builder.Property(e => e.Name).IsRequired();

            builder.Property(e => e.Barcode);

            builder.Property(e => e.IsColdZone).HasDefaultValue(false);

            builder.Property(e => e.IsPrescription).HasDefaultValue(false);

            builder.Property(e => e.IsEnrollPrice).HasDefaultValue(false);

            builder.Property(e => e.Deleted).IsRequired().HasDefaultValue(false);

            builder.Property(e => e.Updated).IsRequired();

            builder.Property(e => e.Created).IsRequired();

            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.ProductTypeId);

            builder.HasIndex(e => e.FactoryId);

            builder.ToTable("Products");

            builder.HasOne(e => e.ProductType)
                .WithMany(e => e.Products)
                .HasForeignKey(e => e.ProductTypeId)
                .HasConstraintName("FK_Product_ProductTypes");

            builder.HasOne(e => e.Factory)
               .WithMany(e => e.Products)
               .HasForeignKey(e => e.FactoryId)
               .HasConstraintName("FK_Product_Factories")
               .IsRequired(false);
        }
    }
}
