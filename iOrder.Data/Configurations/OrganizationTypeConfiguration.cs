﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class OrganizationTypeConfiguration : IEntityTypeConfiguration<OrganizationType>
    {
        public void Configure(EntityTypeBuilder<OrganizationType> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.Type).IsRequired();

            builder.Property(e => e.Deleted).IsRequired().HasDefaultValue(false);

            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.Type);

            builder.ToTable("OrganizationTypes");
        }
    }
}
