﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    class OrderItemStateConfiguration : IEntityTypeConfiguration<OrderItemState>
    {
        public void Configure(EntityTypeBuilder<OrderItemState> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.State).IsRequired();

            builder.Property(e => e.Deleted).IsRequired().HasDefaultValue(false);

            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.State);

            builder.ToTable("OrderItemStates");
        }
    }
}
