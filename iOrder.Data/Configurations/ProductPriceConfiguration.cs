﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class ProductPriceConfiguration : IEntityTypeConfiguration<ProductPrice>
    {
        public void Configure(EntityTypeBuilder<ProductPrice> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.Code).IsRequired();

            builder.Property(e => e.Price).IsRequired();

            builder.Property(e => e.PricePartner).IsRequired();

            builder.Property(e => e.PriceBarter).IsRequired();

            builder.Property(e => e.Expiration).IsRequired(false);

            builder.Property(e => e.ExpirationPartner).IsRequired(false);

            builder.Property(e => e.ExpirationBarter).IsRequired(false);

            builder.Property(e => e.Quantity);

            builder.Property(e => e.QuantityPartner);

            builder.Property(e => e.QuantityBarter);

            builder.Property(e => e.Lot).IsRequired();

            builder.Property(e => e.LotPartner);

            builder.Property(e => e.LotBarter);

            builder.Property(e => e.IsExpectedArrival).HasDefaultValue(false);

            builder.Property(e => e.IsPromo).HasDefaultValue(false);

            builder.Property(e => e.Updated).IsRequired();

            builder.Property(e => e.Created).IsRequired();

            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.ProductId);

            builder.ToTable("ProductPrices");

            builder.HasOne(e => e.Product)
                .WithMany(e => e.ProductPrices)
                .HasForeignKey(e => e.ProductId)
                .HasConstraintName("FK_ProductPrices_Product");
        }
    }
}
