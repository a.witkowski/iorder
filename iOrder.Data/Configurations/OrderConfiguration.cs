﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.Number).IsRequired();

            builder.Property(e => e.Created).IsRequired();

            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.UserId);

            builder.HasIndex(e => e.PointId);

            builder.HasIndex(e => e.Created);

            builder.ToTable("Orders");

            builder.HasOne(e => e.User)
                .WithMany(e => e.Orders)
                .HasForeignKey(e => e.UserId)
                .HasConstraintName("FK_Order_Users");

            builder.HasOne(e => e.Point)
                .WithMany(e => e.Orders)
                .HasForeignKey(e => e.PointId)
                .HasConstraintName("FK_Orders_Point");
        }
    }
}
