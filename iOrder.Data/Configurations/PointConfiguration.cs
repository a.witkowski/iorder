﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class PointConfiguration : IEntityTypeConfiguration<Point>
    {
        public void Configure(EntityTypeBuilder<Point> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.Id1C).IsRequired();

            builder.Property(e => e.Code).IsRequired();

            builder.Property(e => e.Address).IsRequired();

            builder.Property(e => e.Emails);

            builder.Property(e => e.Deleted).HasDefaultValue(false);

            builder.Property(e => e.Updated).IsRequired();

            builder.Property(e => e.Created).IsRequired();

            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.OrganizationId);

            builder.ToTable("Points");

            builder.HasOne(e => e.Organization)
                .WithMany(e => e.Points)
                .HasForeignKey(e => e.OrganizationId)
                .HasConstraintName("FK_Points_Organization");
        }
    }
}
