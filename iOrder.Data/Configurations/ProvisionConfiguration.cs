﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class ProvisionConfiguration : IEntityTypeConfiguration<Provision>
    {
        public void Configure(EntityTypeBuilder<Provision> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.OrganizationId).IsRequired();

            builder.Property(e => e.PointId).IsRequired(false);

            builder.Property(e => e.ManufacturerId).IsRequired();

            builder.Property(e => e.ProductId).IsRequired(false);

            builder.Property(e => e.Price).IsRequired(false);

            builder.Property(e => e.Quantity).IsRequired(false);

            builder.Property(e => e.Start).IsRequired();

            builder.Property(e => e.End).IsRequired();

            builder.Property(e => e.Updated).IsRequired();

            builder.Property(e => e.Created).IsRequired();

            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.OrganizationId);

            builder.HasIndex(e => e.PointId);

            builder.HasIndex(e => e.ManufacturerId);

            builder.HasIndex(e => e.ProductId);

            builder.ToTable("Provisions");

            builder.HasOne(e => e.Organization)
                .WithMany(e => e.Provisions)
                .HasForeignKey(e => e.OrganizationId)
                .HasConstraintName("FK_Provisions_Organization");

            builder.HasOne(e => e.Point)
                .WithMany(e => e.Provisions)
                .HasForeignKey(e => e.PointId)
                .HasConstraintName("FK_Provisions_Point")
                .IsRequired(false);

            builder.HasOne(e => e.Manufacturer)
                .WithMany(e => e.Provisions)
                .HasForeignKey(e => e.ManufacturerId)
                .HasConstraintName("FK_Provisions_Manufacturer");

            builder.HasOne(e => e.Product)
                .WithMany(e => e.Provisions)
                .HasForeignKey(e => e.ProductId)
                .HasConstraintName("FK_Provisions_Product")
                .IsRequired(false);
        }
    }
}
