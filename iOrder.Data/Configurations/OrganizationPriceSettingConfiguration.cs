﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class OrganizationPriceSettingConfiguration : IEntityTypeConfiguration<OrganizationPriceSetting>
    {
        public void Configure(EntityTypeBuilder<OrganizationPriceSetting> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.Id1C).IsRequired();

            builder.Property(e => e.Code).IsRequired();

            builder.Property(e => e.HideWholesale).HasDefaultValue(false);

            builder.Property(e => e.HideRetail).HasDefaultValue(false);

            builder.Property(e => e.HideBarter).HasDefaultValue(false);

            builder.Property(e => e.Deleted).IsRequired().HasDefaultValue(false);

            builder.Property(e => e.Updated).IsRequired();

            builder.Property(e => e.Created).IsRequired();

            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.OrganizationId);

            builder.HasIndex(e => e.FactoryId);

            builder.ToTable("OrganizationPriceSettings");

            builder.HasOne(e => e.Organization)
                .WithMany(e => e.OrganizationPriceSettings)
                .HasForeignKey(e => e.OrganizationId)
                .HasConstraintName("FK_OrganizationPriceSetting_Organizations");

            builder.HasOne(e => e.Factory)
               .WithMany(e => e.OrganizationPriceSettings)
               .HasForeignKey(e => e.FactoryId)
               .HasConstraintName("FK_OrganizationPriceSetting_Factories")
               .IsRequired(false);
        }
    }
}
