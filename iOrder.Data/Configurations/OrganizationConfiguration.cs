﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class OrganizationConfiguration : IEntityTypeConfiguration<Organization>
    {
        public void Configure(EntityTypeBuilder<Organization> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.Id1C).IsRequired();

            builder.Property(e => e.Code).IsRequired();

            builder.Property(e => e.Name).IsRequired();

            builder.Property(e => e.Deleted).IsRequired().HasDefaultValue(false);

            builder.Property(e => e.Updated).IsRequired();

            builder.Property(e => e.Created).IsRequired();

            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.OrganizationTypeId);

            builder.ToTable("Organizations");

            builder.HasOne(e => e.OrganizationType)
                .WithMany(e => e.Organizations)
                .HasForeignKey(e => e.OrganizationTypeId)
                .HasConstraintName("FK_Organizations_OrganizationTypes");

            builder.HasOne(e => e.OrganizationSetting)
                .WithOne(e => e.Organization)
                .HasForeignKey<OrganizationSetting>(e => e.Id);
        }
    }
}
