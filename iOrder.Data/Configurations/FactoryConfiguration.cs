﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class FactoryConfiguration : IEntityTypeConfiguration<Factory>
    {
        public void Configure(EntityTypeBuilder<Factory> builder)
        {
            builder.Property(e => e.Id).ValueGeneratedOnAdd();

            builder.Property(e => e.Id1C).IsRequired();

            builder.Property(e => e.Code).IsRequired();

            builder.Property(e => e.Name).IsRequired();

            builder.Property(e => e.Country);

            builder.Property(e => e.Deleted).HasDefaultValue(false);

            builder.Property(e => e.Updated).IsRequired();

            builder.Property(e => e.Created).IsRequired();

            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.ManufacturerId);

            builder.ToTable("Factories");

            builder.HasOne(e => e.Manufacturer)
                .WithMany(e => e.Factories)
                .HasForeignKey(e => e.ManufacturerId)
                .HasConstraintName("FK_Factory_Manufacturers")
                .IsRequired(false);
        }
    }
}
