﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace iOrder.Data.Configurations
{
    public class UserPointConfiguration : IEntityTypeConfiguration<UserPoint>
    {
        public void Configure(EntityTypeBuilder<UserPoint> builder)
        {
            builder.Property(e => e.UserId).IsRequired().IsConcurrencyToken();

            builder.Property(e => e.PointId).IsRequired().IsConcurrencyToken();

            builder.ToTable("UserPoints");

            builder.HasKey(e => new { e.UserId, e.PointId });

            builder.HasOne(e => e.User)
                .WithMany(e => e.UserPoints)
                .HasForeignKey(e => e.UserId);

            builder.HasOne(e => e.Point)
                .WithMany(e => e.UserPoints)
                .HasForeignKey(e => e.PointId);
        }
    }
}
