﻿using iOrder.Application.Emails;
using iOrder.Application.Emails.Models;
using iOrder.Application.Interfaces;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Logging;
using MimeKit;
using System;
using System.IO;
using System.Threading.Tasks;

namespace iOrder.Infrastructure
{
    public class EmailServices : IEmailServices
    {
        const string EMAILS_TEMPLATES_FOLDER = "templates//emails";

        private readonly ILogger<EmailServices> _logger;
        private readonly EmailSettings _emailSettings;

        public EmailServices(ILogger<EmailServices> logger, EmailSettings emailSettings)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _emailSettings = emailSettings ?? throw new ArgumentNullException(nameof(emailSettings));
        }

        public async Task<string> GetTemplateAsync(EmailTemplate template)
        {
            string filePath;
            switch (template)
            {
                case EmailTemplate.OrderConfirmation:
                    filePath = Path.Combine(EMAILS_TEMPLATES_FOLDER, nameof(EmailTemplate.OrderConfirmation));
                    break;
                default:
                    _logger.LogError("Email template not found");
                    throw new Exception("Email template not found");
            }

            using (StreamReader SourceReader = File.OpenText(filePath))
            {
                return await SourceReader.ReadToEndAsync();
            }
        }

        public async Task SendAsync(MessageDto message)
        {
            try
            {
                _logger.LogInformation("EmailServices Start");

                var emailMessage = new MimeMessage();

                if (message.Bcc != null && message.Bcc.Count > 0)
                {
                    foreach (var item in message.Bcc)
                    {
                        _logger.LogInformation($"Email Bcc: {item}");
                        emailMessage.Bcc.Add(new MailboxAddress(item));
                    }
                }

                if (message.Cc != null && message.Cc.Count > 0)
                {
                    foreach (var item in message.Cc)
                    {
                        _logger.LogInformation($"Email Cc: {item}");
                        emailMessage.Cc.Add(new MailboxAddress(item));
                    }
                }

                if (message.To != null && message.To.Count > 0)
                {
                    foreach (var item in message.To)
                    {
                        _logger.LogInformation($"Email To: {item}");
                        emailMessage.To.Add(new MailboxAddress(item));
                    }
                }

                _logger.LogInformation($"Email From: {message.From}");
                emailMessage.From.Add(new MailboxAddress(message.From));

                _logger.LogInformation($"Email Subject: {message.Subject}");
                emailMessage.Subject = message.Subject;

                emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
                {
                    Text = message.Body
                };

                using (var client = new SmtpClient())
                {
                    await client.ConnectAsync(_emailSettings.Host, _emailSettings.Port, false);
                    await client.AuthenticateAsync(_emailSettings.User, _emailSettings.Password);
                    await client.SendAsync(emailMessage);

                    await client.DisconnectAsync(true);
                }

                _logger.LogInformation("EmailServices End");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
