﻿using iOrder.Application.Infrastructure.FTP.Models;
using iOrder.Application.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace iOrder.Infrastructure
{
    public class FTPServices : IFTPServices
    {
        private readonly ILogger<FTPServices> _logger;
        private readonly IOptions<FTPSettings> _fTPSettings;

        public FTPServices(
            ILogger<FTPServices> logger,
            IOptions<FTPSettings> fTPSettings)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _fTPSettings = fTPSettings ?? throw new ArgumentNullException(nameof(fTPSettings));
        }

        public async Task UploadAsync(string fileName, byte[] fileContent)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create($"{_fTPSettings.Value.Host}/{_fTPSettings.Value.Folder}/{fileName}");
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(_fTPSettings.Value.User, _fTPSettings.Value.Password);
                request.ContentLength = fileContent.Length;

                using (Stream requestStream = request.GetRequestStream())
                {
                    await requestStream.WriteAsync(fileContent, 0, fileContent.Length);
                }
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, ex.Message, ex.InnerException);
            }
        }
    }
}
