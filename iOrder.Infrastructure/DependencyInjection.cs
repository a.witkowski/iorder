﻿using iOrder.Application.Emails.Models;
using iOrder.Application.Infrastructure.FTP.Models;
using iOrder.Application.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace iOrder.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            //services.Configure<FTPSettings>(options => configuration.GetSection("FTPSettings").Bind(options));
            //services.Configure<EmailSettings>(options => configuration.GetSection("EmailSettings").Bind(options));

            //services.AddSingleton<FTPSettings, FTPSettings>();
            //services.AddSingleton<EmailSettings, EmailSettings>();
            //var emailSettings = new EmailSettings();
            //configuration.GetSection("EmailSettings").Bind(emailSettings);
            //EmailSettings = emailSettings;

            //services.AddTransient<IEmailServices, EmailServices>();
            services.AddTransient<IFTPServices, FTPServices>();

            return services;
        }
    }
}
