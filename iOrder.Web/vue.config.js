const webpack = require("webpack");

module.exports = {
  transpileDependencies: [/(\/|\\)vuetify(\/|\\)/, "resize-detector"],

  lintOnSave: process.env.NODE_ENV !== "production",
  configureWebpack: {
    // optimization: {
    //   splitChunks: {
    // chunks: 'all'
    //     minSize: 10000,
    //     maxSize: 250000
    //   }
    // },
    plugins: [
      new webpack.ProvidePlugin({
        introJs: ["intro.js"]
      })
    ]
  }
};
