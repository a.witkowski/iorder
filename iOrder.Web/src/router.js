import Vue from "vue";
import Router from "vue-router";

import { Role } from "@/helpers";

const Home = () => import("@/views/Home");
const Login = () => import("@/views/Login");
const Orders = () => import("@/views/Orders");
const Order = () => import("@/views/Order");
const Users = () => import("@/views/Users");
const Settings = () => import("@/views/Settings");
const Promotions = () => import("@/views/Promotions");
const Provisions = () => import("@/views/Provisions");
const NotFound = () => import("@/views/NotFound.vue");

Vue.use(Router);

const routes = [
  { path: "/index.html", component: Home, alias: "/", meta: { authorize: [] } },
  {
    path: "/",
    name: "home",
    component: Home,
    meta: { authorize: [] },
  },
  {
    path: "/login",
    name: "login",
    component: Login,
  },
  {
    path: "/orders",
    name: "orders",
    component: Orders,
    meta: { authorize: [Role.Manager, Role.User] },
  },
  {
    path: "/order/:id?",
    name: "order",
    component: Order,
    meta: { authorize: [Role.Manager, Role.User] },
    props: true,
  },
  {
    path: "/provisions",
    name: "provisions",
    component: Provisions,
    meta: { authorize: [Role.Manager, Role.User] },
  },
  {
    path: "/users",
    name: "users",
    component: Users,
    meta: { authorize: [Role.Manager] },
  },
  {
    path: "/promotions",
    name: "promotions",
    component: Promotions,
    meta: { authorize: [] },
  },
  {
    path: "/settings",
    name: "settings",
    component: Settings,
    meta: { authorize: [Role.Admin, Role.Manager] },
  },
  { path: "/notfound", component: NotFound },
  { path: "*", redirect: "/notfound" },
];

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: routes,
});

router.beforeEach((to, from, next) => {
  const { authorize } = to.meta;

  const token = JSON.parse(localStorage.getItem("token"));

  if (authorize) {
    if (!token || !token.token) {
      return next({ path: "/login", query: { returnUrl: to.path } });
    }

    const foundRole = authorize.some((i) => token.roles.includes(i));
    if (authorize.length && !foundRole) {
      return next({ path: "/" });
    }
  }

  next();
});

export default router;
