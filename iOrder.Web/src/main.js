import "babel-polyfill";
import Vue from "vue";
import store from "@/store/store";
import router from "./router";
import "./registerServiceWorker";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import vuetify from "@/plugins/vuetify";
import "@/styles/index.scss";
import "@/plugins/dayjs";
import "@/plugins/echarts";
import "@/plugins/introjs";
import "@/plugins/jsonExcel";
import "@/plugins/sticky";
import "@/filters/date";
import "@/filters/money";
import App from "./App.vue";

import { httpApi } from "@/api/api.config";

Vue.config.productionTip = false;

const key = "token";
const token = JSON.parse(localStorage.getItem(key));
if (token)
  httpApi.defaults.headers.common["Authorization"] = "Bearer " + token.token;

new Vue({
  store,
  router,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
