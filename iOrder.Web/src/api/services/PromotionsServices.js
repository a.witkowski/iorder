import { httpApi } from "../api.config";

const getPromotions = async () => {
  return await httpApi
    .get("/promotions/get")
    .then(async (response) => {
      return await response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const getValidPromotions = async (userId) => {
  return await httpApi
    .get("/promotions/getvalid/" + userId)
    .then(async (response) => {
      return await response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const createPromotion = async (data) => {
  return await httpApi
    .post("/promotions/create", data)
    .then(async (response) => {
      return await response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const updatePromotion = async (data) => {
  return await httpApi
    .put("/promotions/update", data)
    .then(async (response) => {
      return await response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const deletePromotion = async (data) => {
  return await httpApi
    .delete("/promotions/delete/" + data)
    .then(async (response) => {
      return await response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

export default {
  getPromotions: getPromotions,
  getValidPromotions: getValidPromotions,
  createPromotion: createPromotion,
  updatePromotion: updatePromotion,
  deletePromotion: deletePromotion,
};
