import { httpApi } from "../api.config";

const getBasket = async (userId) => {
  return await httpApi
    .get("/baskets/get/" + userId)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const upsertBasket = async (data) => {
  return await httpApi
    .post("/baskets/upsert", data)
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const deleteBasket = async (data) => {
  return await httpApi
    .delete("/baskets/delete", { params: data })
    .then(async (response) => {
      return await response.data;
    })
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

export default {
  getBasket: getBasket,
  upsertBasket: upsertBasket,
  deleteBasket: deleteBasket,
};
