import { httpApi } from "../api.config";

const getProvisions = async (userId) => {
  return await httpApi
    .get("/provisions/get/" + userId)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const getStatistics = async (data) => {
  return await httpApi
    .get("/provisions/getstatistics", { params: data })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const createProvision = async (data) => {
  return await httpApi
    .post("/provisions/create", data)
    .then(async (response) => {
      return await response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const deleteProvision = async (data) => {
  return await httpApi
    .delete("/provisions/delete/" + data)
    .then(async (response) => {
      return await response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

export default {
  getProvisions: getProvisions,
  getStatistics: getStatistics,
  createProvision: createProvision,
  deleteProvision: deleteProvision,
};
