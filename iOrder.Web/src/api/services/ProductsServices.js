import { httpApi } from "../api.config";

const getProductPrices = async (userId) => {
  return await httpApi
    .get("/products/getproductprices/" + userId)
    .then(async (response) => {
      return await response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const getProductTypes = async (userId) => {
  return await httpApi
    .get("/products/getproducttypes/" + userId)
    .then(async (response) => {
      return await response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const getProductsState = async (userId) => {
  return await httpApi
    .get("/products/getproductsstate/" + userId)
    .then(async (response) => {
      return await response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const getValidManufacturersProducts = async () => {
  return await httpApi
    .get("/products/getvalidmanufacturersproducts")
    .then(async (response) => {
      return await response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

export default {
  getProductPrices: getProductPrices,
  getProductTypes: getProductTypes,
  getProductsState: getProductsState,
  getValidManufacturersProducts: getValidManufacturersProducts,
};
