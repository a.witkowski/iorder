import { httpApi } from "../api.config";

const getPoints = async (userId) => {
  return await httpApi
    .get("/points/get/" + userId)
    .then(async (response) => {
      return await response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const getChildUsersPoints = async (userId) => {
  return await httpApi
    .get("/points/getchilduserspoints/" + userId)
    .then(async (response) => {
      return await response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const createChildUserPoint = async (data) => {
  return await httpApi
    .post("/points/createchilduserpoint", data)
    .then(async (response) => {
      return await response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const deleteChildUserPoint = async (data) => {
  return await httpApi
    .delete("/points/deletechilduserpoint", { params: data })
    .then(async (response) => {
      return await response.data;
    })
    .catch((error) => {
      console.log(error);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

export default {
  getPoints: getPoints,
  getChildUsersPoints: getChildUsersPoints,
  createChildUserPoint: createChildUserPoint,
  deleteChildUserPoint: deleteChildUserPoint,
};
