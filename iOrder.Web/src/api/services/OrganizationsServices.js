import { httpApi } from "../api.config";

const getOrganizations = async () => {
  return await httpApi
    .get("/organizations/getorganizations")
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const getOrganizationSettings = async (userId) => {
  return await httpApi
    .get("/organizations/getorganizationsettings/" + userId)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const getOrganizationsSettings = async () => {
  return await httpApi
    .get("/organizations/getorganizationssettings")
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const getContracts = async (userId) => {
  return await httpApi
    .get("/organizations/getcontracts/" + userId)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const getOrganizationUsers = async (organizationId) => {
  return await httpApi
    .get("/organizations/getorganizationusers/" + organizationId)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const updateOrganizationSettings = async (data) => {
  return await httpApi
    .put("/organizations/updateorganizationsettings", data)
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

export default {
  getOrganizations: getOrganizations,
  getOrganizationSettings: getOrganizationSettings,
  getOrganizationsSettings: getOrganizationsSettings,
  getContracts: getContracts,
  getOrganizationUsers: getOrganizationUsers,
  updateOrganizationSettings: updateOrganizationSettings,
};
