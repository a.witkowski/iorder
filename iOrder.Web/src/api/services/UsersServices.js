import { httpApi } from "../api.config";

const login = async (login, password) => {
  return await httpApi
    .post("/users/login", JSON.stringify({ login: login, password: password }))
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const getUser = async (id) => {
  return await httpApi
    .get("/users/get/" + id)
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const getChildUsers = async (parentUserId) => {
  return await httpApi
    .get("/users/getchildusers/" + parentUserId)
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const createChildUser = async (data) => {
  return await httpApi
    .post("/users/createchilduser", data)
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const updateChildUser = async (data) => {
  return await httpApi
    .put("/users/updatechilduser", data)
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const updateUserSettings = async (data) => {
  return await httpApi
    .put("/users/updateusersettings", data)
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

export default {
  login: login,
  getUser: getUser,
  getChildUsers: getChildUsers,
  createChildUser: createChildUser,
  updateChildUser: updateChildUser,
  updateUserSettings: updateUserSettings,
};
