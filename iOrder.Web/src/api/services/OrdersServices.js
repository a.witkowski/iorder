import { httpApi } from "../api.config";

const getOrders = async (userId) => {
  return await httpApi
    .get("/orders/get/" + userId)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const getOrderItems = async (userId, orderId) => {
  return await httpApi
    .get("/orders/getorderitems?userId=" + userId + "&orderId=" + orderId)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const getOrdersItemsQuantities = async (userId, month, year) => {
  return await httpApi
    .get(
      "/orders/getordersitemsquantities?userId=" +
        userId +
        "&month=" +
        month +
        "&year=" +
        year
    )
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

const createOrder = async (data) => {
  return await httpApi
    .post("/orders/create", data)
    .catch((error) => {
      console.log(error.response);
      return Promise.reject(error.response.data);
    })
    .finally(() => {});
};

export default {
  getOrders: getOrders,
  getOrderItems: getOrderItems,
  getOrdersItemsQuantities: getOrdersItemsQuantities,
  createOrder: createOrder,
};
