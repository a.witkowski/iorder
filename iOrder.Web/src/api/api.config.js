import axios from "axios";

const urlApi = process.env.VUE_APP_API_URL || "https://localhost/api";

const headers = {
  "Content-Type": "application/json",
};

export const httpApi = axios.create({
  baseURL: urlApi,
  headers: headers,
});
