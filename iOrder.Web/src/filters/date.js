import Vue from "vue";
import dayjs from "dayjs";

Vue.filter("datetime", function (value) {
  if (value) {
    return dayjs(String(value)).format("DD.MM.YYYY HH:mm");
  }
});

Vue.filter("date", function (value) {
  if (value) {
    return dayjs(String(value)).format("DD.MM.YYYY");
  }
});

Vue.filter("expiration", function (value) {
  if (value) {
    return dayjs(String(value)).format("MM-YYYY");
  }
});
