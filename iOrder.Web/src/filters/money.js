import Vue from "vue";

Vue.filter("twoDecimalPoint", function (value) {
  if (value) {
    return value.toFixed(2);
  }
});
