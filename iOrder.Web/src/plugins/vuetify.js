import Vue from "vue";
import Vuetify from "vuetify/lib";
import ru from "vuetify/es5/locale/ru";
// import en from "vuetify/es5/locale/en";
import colors from "vuetify/es5/util/colors";

const opts = {
  theme: {
    themes: {
      light: {
        primary: colors.green,
        secondary: colors.orange.lighten3, //colors.amber,
        tertiary: colors.orange.lighten5,
        accent: colors.green.accent3,
        warning: colors.orange.lighten3,
        error: colors.deepOrange.lighten2,
      },
    },
  },
  icons: {
    iconfont: "mdiSvg",
  },
  lang: {
    locales: { ru },
    current: "ru",
  },
};

Vue.use(Vuetify);

export default new Vuetify(opts);
