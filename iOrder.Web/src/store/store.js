import Vue from "vue";
import Vuex from "vuex";

import logs from "./modules/logs";
import users from "./modules/users";
import products from "./modules/products";
import orders from "./modules/orders";
import points from "./modules/points";
import organizations from "./modules/organizations";
import promotions from "./modules/promotions";
import provisions from "./modules/provisions";
import baskets from "./modules/baskets";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    logs,
    users,
    products,
    orders,
    points,
    organizations,
    promotions,
    provisions,
    baskets,
  },
  strict: process.env.NODE_ENV !== "production",
});
