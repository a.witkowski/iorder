import promotionsServices from "@/api/services/PromotionsServices";

const state = {
  promotions: Array(),
  validPromotions: Array(),
};

const getters = {};

const actions = {
  GET_PROMOTIONS: async (context) => {
    await promotionsServices.getPromotions().then((result) => {
      context.commit("SET_PROMOTIONS", result);
    });
  },
  GET_VALID_PROMOTIONS: async (context, userId) => {
    await promotionsServices.getValidPromotions(userId).then((result) => {
      context.commit("SET_VALID_PROMOTIONS", result);
    });
  },
  ADD_PROMOTION: async (context, payload) => {
    const formData = new FormData();
    formData.append("organizationTypeId", payload.organizationTypeId);
    formData.append("organizationId", payload.organizationId);
    formData.append("file", payload.file);
    formData.append("fileName", payload.name);
    formData.append("start", payload.start);
    formData.append("end", payload.end);
    await promotionsServices.createPromotion(formData);
  },
  UPDATE_PROMOTION: async (context, payload) => {
    await promotionsServices.updatePromotion(payload);
  },
  REMOVE_PROMOTION: async (context, payload) => {
    await promotionsServices.deletePromotion(payload.promotionId).then(() => {
      context.commit("REMOVE_PROMOTION", payload);
    });
  },
};

const mutations = {
  SET_PROMOTIONS: (state, payload) => {
    state.promotions = payload;
  },
  SET_VALID_PROMOTIONS: (state, payload) => {
    state.validPromotions = payload;
  },
  REMOVE_PROMOTION: (state, payload) => {
    const index = state.promotions.indexOf(payload);
    if (index >= 0) state.promotions.splice(index, 1);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
