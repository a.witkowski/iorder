import productsServices from "@/api/services/ProductsServices";

const state = {
  productPrices: Array(),
  productTypes: Array(),
  selectedProductTypes: Object(),
  selectedColdZone: false,
  selectedCountryBelarus: false,
  productsState: Array(),
  validManufacturersProducts: Array(),
};

const getters = {};

const actions = {
  GET_PRODUCT_PRICES: async (context) => {
    // context.commit("points/CLEAR_SELECTED_POINTS", null, { root: true });

    await productsServices
      .getProductPrices(context.rootState.users.user.userId)
      .then((result) => {
        context.commit("SET_PRODUCT_PRICES", result);
      });
  },
  GET_PRODUCT_TYPES: async (context) => {
    await productsServices
      .getProductTypes(context.rootState.users.user.userId)
      .then((result) => {
        context.commit("SET_PRODUCT_TYPES", result);
      });
  },
  UPDATE_SELECTED_PRODUCT_TYPES: async (context, payload) => {
    if (payload.value) context.commit("ADD_SELECTED_PRODUCT_TYPES", payload);
    else context.commit("REMOVE_SELECTED_PRODUCT_TYPES", payload);
  },
  UPDATE_SELECTED_COLD_ZONE: async (context, payload) => {
    context.commit("EDIT_SELECTED_COLD_ZONE", payload);
  },
  UPDATE_SELECTED_COUNTRY_BELARUS: async (context, payload) => {
    context.commit("EDIT_SELECTED_COUNTRY_BELARUS", payload);
  },
  GET_PRODUCTS_STATE: async (context) => {
    await productsServices
      .getProductsState(context.rootState.users.user.userId)
      .then((result) => {
        context.commit("SET_PRODUCTS_STATE", result);
      });
  },
  GET_VALID_MANUFACTURERS_PRODUCTS: async (context) => {
    await productsServices.getValidManufacturersProducts().then((result) => {
      context.commit("SET_VALID_MANUFACTURERS_PRODUCTS", result);
    });
  },
};

const mutations = {
  SET_PRODUCT_PRICES: (state, payload) => {
    state.productPrices = payload;
  },
  SET_PRODUCT_TYPES: (state, payload) => {
    state.productTypes = payload;
  },
  ADD_SELECTED_PRODUCT_TYPES: (state, payload) => {
    state.selectedProductTypes[payload.productTypeId] = payload;
  },
  REMOVE_SELECTED_PRODUCT_TYPES: (state, payload) => {
    delete state.selectedProductTypes[payload.productTypeId];
  },
  EDIT_SELECTED_COLD_ZONE: (state, payload) => {
    state.selectedColdZone = payload;
  },
  EDIT_SELECTED_COUNTRY_BELARUS: (state, payload) => {
    state.selectedCountryBelarus = payload;
  },
  SET_PRODUCTS_STATE: (state, payload) => {
    state.productsState = payload;
  },
  SET_VALID_MANUFACTURERS_PRODUCTS: (state, payload) => {
    state.validManufacturersProducts = payload;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
