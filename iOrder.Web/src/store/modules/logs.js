const state = {
  isError: false,
  errorObj: "",
  errorMsg: "",
};

const getters = {};

const actions = {
  LOG: async (context, payload) => {
    context.commit("SET_ERROR");
    context.commit("SET_ERROR_MESSAGE", payload.error);
    context.commit("SET_ERROR_OBJECT", payload);
  },
};

const mutations = {
  SET_ERROR: (state) => {
    state.isError = !state.isError;
  },
  SET_ERROR_MESSAGE: (state, payload) => {
    state.errorMsg = payload;
  },
  SET_ERROR_OBJECT: (state, payload) => {
    state.errorObj = { ...payload };
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
