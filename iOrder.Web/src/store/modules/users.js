import usersServices from "@/api/services/UsersServices";

import { httpApi } from "@/api/api.config";

const key = "token";

let token = JSON.parse(localStorage.getItem(key));
let tokenUserId = token != null ? token.userId : null;

const state = {
  user: Object(),
  childUsers: Array(),
};

const getters = {};

const actions = {
  LOGIN: async (context, payload) => {
    if (context.state.user.userId == null) {
      await usersServices
        .login(payload.user, payload.password)
        .then(async (result) => {
          await context.commit("SET_TOKEN", result.data);
          await context.dispatch("GET_USER", result.data.userId);
        })
        .catch((error) => {
          console.log(error);
          context.dispatch("logs/LOG", error, { root: true });
          context.commit("CLEAR_USER");
        });
    }
  },
  LOGOUT: async (context) => {
    context.commit("CLEAR_USER");
  },
  GET_USER: async (context, userId) => {
    const id = userId || tokenUserId;
    if (id && id != null) {
      await usersServices
        .getUser(id)
        .then((result) => {
          context.commit("SET_USER", result.data);
        })
        .catch((error) => {
          context.dispatch("logs/LOG", error, { root: true });
        });
    }
  },
  GET_CHILD_USERS: (context) => {
    if (context.state.user != null) {
      usersServices
        .getChildUsers(context.state.user.userId)
        .then((result) => {
          context.commit("SET_CHILD_USERS", result.data);
        })
        .catch((error) => {
          console.log(error);
          context.dispatch("logs/LOG", error, { root: true });
        });
    }
  },
  CREATE_CHILD_USER: async (context, payload) => {
    if (context.state.user.userId != null && payload != null) {
      payload.parentId = context.state.user.userId;
      await usersServices
        .createChildUser(payload)
        .then(() => {
          context.dispatch("GET_CHILD_USERS");
        })
        .catch((error) => {
          console.log(error);
          context.dispatch("logs/LOG", error, { root: true });
        });
    }
  },
  UPDATE_CHILD_USER: async (context, payload) => {
    if (context.state.user.userId != null && payload != null) {
      payload.parentId = context.state.user.userId;
      await usersServices
        .updateChildUser(payload)
        .then(() => {
          context.dispatch("GET_CHILD_USERS");
        })
        .catch((error) => {
          console.log(error);
          context.dispatch("logs/LOG", error, { root: true });
        });
    }
  },
  UPDATE_USER_SETTINGS: async (context, payload) => {
    await usersServices
      .updateUserSettings({
        userId: payload.userId,
        [payload.key]: payload.value,
      })
      .then()
      .catch((e) => {
        console.log(e);
        context.dispatch("logs/LOG", e, { root: true });
      });
  },
};

const mutations = {
  SET_TOKEN: (state, payload) => {
    localStorage.setItem(key, JSON.stringify(payload));
    httpApi.defaults.headers.common["Authorization"] =
      "Bearer " + payload.token;
  },
  SET_USER: (state, payload) => {
    state.user = payload;
  },
  SET_CHILD_USERS: (state, payload) => {
    state.childUsers = payload;
  },
  CLEAR_USER: (state) => {
    localStorage.removeItem(key);
    tokenUserId = null;
    token = null;
    state.user = new Object();
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
