import pointsServices from "@/api/services/PointsServices";

const state = {
  points: [],
  selectedPoints: [],
  childUsersPoints: [],
};

const getters = {};

const actions = {
  GET_POINTS: async (context, userId) => {
    await pointsServices.getPoints(userId).then((result) => {
      context.commit("SET_POINTS", result);

      if (result.length == 1) {
        context.commit("SET_SELECTED_POINTS", result);
      }
    });
  },
  ADD_SELECTED_POINTS: async (context, payload) => {
    context.commit("SET_SELECTED_POINTS", payload);
  },
  REMOVE_SELECTED_POINT: async (context, payload) => {
    context.commit("REMOVE_SELECTED_POINT", payload);
  },
  GET_CHILD_USERS_POINTS: async (context, userId) => {
    await pointsServices.getChildUsersPoints(userId).then((result) => {
      context.commit("SET_CHILD_USERS_POINTS", result);
    });
  },
  CREATE_CHILD_USER_POINT: async (context, payload) => {
    const userPoints = context.state.childUsersPoints.filter(
      (item) => item.userId === payload.userId
    );

    for (let index = 0; index < payload.points.length; index++) {
      const point = payload.points[index];
      const currentPoint =
        userPoints && userPoints != null && userPoints.length > 0
          ? userPoints[0].points.filter(
              (item) => item.pointId === point.pointId
            )
          : [];
      if (!currentPoint || currentPoint == null || currentPoint.length == 0) {
        const data = { userId: payload.userId, pointId: point.pointId };
        await pointsServices.createChildUserPoint(data).then(() => {
          context.commit("ADD_CHILD_USER_POINT", payload);
        });
      }
    }
  },
  REMOVE_CHILD_USER_POINT: async (context, payload) => {
    await pointsServices.deleteChildUserPoint(payload).then(() => {
      context.commit("REMOVE_CHILD_USER_POINT", payload);
    });
  },
};

const mutations = {
  SET_POINTS: (state, payload) => {
    state.points = payload;
  },
  SET_SELECTED_POINTS: (state, payload) => {
    state.selectedPoints = payload;
  },
  REMOVE_SELECTED_POINT: (state, payload) => {
    const index = state.selectedPoints.indexOf(
      state.selectedPoints.find((item) => {
        return item.pointId == payload.pointId;
      })
    );
    if (index >= 0) state.selectedPoints.splice(index, 1);
  },
  CLEAR_SELECTED_POINTS: (state) => {
    state.selectedPoints = {};
  },
  SET_CHILD_USERS_POINTS: (state, payload) => {
    state.childUsersPoints = payload;
  },
  ADD_CHILD_USER_POINT: (state, payload) => {
    const points = state.childUsersPoints.filter(
      (item) => item.userId === payload.userId
    );

    if (
      state.childUsersPoints &&
      state.childUsersPoints != null &&
      points &&
      points != null &&
      points.length > 0
    ) {
      state.childUsersPoints.filter(
        (item) => item.userId === payload.userId
      )[0].points = payload.points;
    } else {
      state.childUsersPoints.push(payload);
    }
  },
  REMOVE_CHILD_USER_POINT: (state, payload) => {
    const points = state.childUsersPoints.filter(
      (item) => item.userId === payload.userId
    )[0].points;

    const index = points.findIndex((item) => item.pointId === payload.pointId);
    if (index >= 0)
      state.childUsersPoints
        .filter((item) => item.userId === payload.userId)[0]
        .points.splice(index, 1);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
