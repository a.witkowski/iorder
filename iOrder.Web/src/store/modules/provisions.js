import provisionsServices from "@/api/services/ProvisionsServices";

const state = {
  provisions: Array(),
  statistics: Array(),
};

const getters = {};

const actions = {
  GET_PROVISIONS: async (context) => {
    await provisionsServices
      .getProvisions(context.rootState.users.user.userId)
      .then((result) => {
        context.commit("SET_PROVISIONS", result);
      });
  },
  GET_STATISTICS: async (context, payload) => {
    await provisionsServices.getStatistics(payload).then((result) => {
      context.commit("SET_STATISTICS", result);
    });
  },
  ADD_PROVISION: async (context, payload) => {
    payload.userId = context.rootState.users.user.userId;
    await provisionsServices.createProvision(payload);
  },
  REMOVE_PROVISION: async (context, payload) => {
    await provisionsServices.deleteProvision(payload).then(() => {
      context.commit("REMOVE_PROVISION", payload);
    });
  },
};

const mutations = {
  SET_PROVISIONS: (state, payload) => {
    state.provisions = payload;
  },
  SET_STATISTICS: (state, payload) => {
    state.statistics = payload;
  },
  REMOVE_PROVISION: (state, payload) => {
    const index = state.provisions.indexOf(payload);
    if (index >= 0) state.provisions.splice(index, 1);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
