import basketsServices from "@/api/services/BasketsServices";

const state = {
  basket: [],
};

const getters = {
  BASKET_TOTAL: (state) => {
    let total = 0;
    state.basket.forEach(function (item) {
      if (item != null) total += item.quantity * item.price;
    });

    return total;
  },
};

const actions = {
  GET_BASKET: async (context, payload) => {
    await basketsServices
      .getBasket(context.rootState.users.user.userId, payload)
      .then((result) => {
        context.commit("SET_BASKET", result);

        if (result && result != null && result.length > 0) {
          result.forEach((item) => {
            context.dispatch("orders/UPDATE_SELECTED_ORDER_ITEMS", item, {
              root: true,
            });
          });
        }
      });
  },
  UPSERT_BASKET: async (context, payload) => {
    if (context.rootState.users.user.userId != null && payload != null) {
      payload.userId = context.rootState.users.user.userId;
      await basketsServices
        .upsertBasket(payload)
        .then(() => {
          context.dispatch("GET_BASKET");
        })
        .catch((error) => {
          console.log(error);
          context.dispatch("logs/LOG", error, { root: true });
        });
    }
  },
  DELETE_BASKET: async (context, payload) => {
    await basketsServices
      .deleteBasket({ userId: context.rootState.users.user.userId })
      .then(() => {
        context.dispatch("orders/DELETE_SELECTED_ORDER_ITEMS", null, {
          root: true,
        });
        context.dispatch("GET_BASKET");
      })
      .catch((error) => {
        console.log(error);
        context.dispatch("logs/LOG", error, { root: true });
      });
  },
};

const mutations = {
  SET_BASKET: (state, payload) => {
    state.basket = payload;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
