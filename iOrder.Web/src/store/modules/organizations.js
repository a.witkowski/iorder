import organizationsServices from "@/api/services/OrganizationsServices";

const state = {
  organizations: [],
  organizationSettings: {},
  contracts: [],
  organizationUsers: [],
  organizationsSettings: [],
};

const getters = {};

const actions = {
  GET_ORGANIZATIONS: async (context) => {
    await organizationsServices.getOrganizations().then((result) => {
      context.commit("SET_ORGANIZATIONS", result);
    });
  },
  GET_ORGANIZATION_SETTINGS: async (context) => {
    await organizationsServices
      .getOrganizationSettings(context.rootState.users.user.userId)
      .then((result) => {
        context.commit("SET_ORGANIZATION_SETTINGS", result);
      });
  },
  GET_ORGANIZATIONS_SETTINGS: async (context) => {
    await organizationsServices.getOrganizationsSettings().then((result) => {
      context.commit("SET_ORGANIZATIONS_SETTINGS", result);
    });
  },
  GET_CONTRACTS: async (context) => {
    await organizationsServices
      .getContracts(context.rootState.users.user.userId)
      .then((result) => {
        context.commit("SET_CONTRACTS", result);
      });
  },
  GET_ORGANIZATION_USERS: async (context) => {
    await organizationsServices
      .getOrganizationUsers(context.rootState.users.user.organizationId)
      .then((result) => {
        context.commit("SET_ORGANIZATION_USERS", result);
      });
  },
  UPDATE_ORGANIZATION_SETTINGS: async (context, payload) => {
    await organizationsServices
      .updateOrganizationSettings({
        organizationId: payload.organizationId,
        [payload.key]: payload.value,
      })
      .then()
      .catch((e) => {
        console.log(e);
        context.dispatch("logs/LOG", e, { root: true });
      });
  },
};

const mutations = {
  SET_ORGANIZATIONS: (state, payload) => {
    state.organizations = payload;
  },
  SET_ORGANIZATION_SETTINGS: (state, payload) => {
    state.organizationSettings = payload;
  },
  SET_ORGANIZATIONS_SETTINGS: (state, payload) => {
    state.organizationsSettings = payload;
  },
  SET_CONTRACTS: (state, payload) => {
    state.contracts = payload;
  },
  SET_ORGANIZATION_USERS: (state, payload) => {
    state.organizationUsers = payload;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
