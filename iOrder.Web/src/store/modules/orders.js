import Vue from "vue";
import ordersServices from "@/api/services/OrdersServices";

const state = {
  orders: [],
  orderItems: [],
  ordersItemsQuantities: [],
  selectedOrderItems: {},
  filterOrders: {},
};

const getters = {};

const actions = {
  GET_ORDERS: async (context) => {
    await ordersServices
      .getOrders(context.rootState.users.user.userId)
      .then((result) => {
        context.commit("SET_ORDERS", result);
      });
  },
  GET_ORDER_ITEMS: async (context, payload) => {
    await ordersServices
      .getOrderItems(context.rootState.users.user.userId, payload)
      .then((result) => {
        context.commit("SET_ORDER_ITEMS", result);
      });
  },
  GET_ORDER_ITEMS_QUANTITIES: async (context, payload) => {
    await ordersServices
      .getOrdersItemsQuantities(
        context.rootState.users.user.userId,
        payload && payload.month && payload.month != null ? payload.month : "",
        payload && payload.year && payload.year != null ? payload.year : ""
      )
      .then((result) => {
        context.commit("SET_ORDER_ITEMS_QUANTITIES", result);
      });
  },
  UPDATE_SELECTED_ORDER_ITEMS: async (context, payload) => {
    context.commit("EDIT_SELECTED_ORDER_ITEM", payload);
  },
  CREATE_ORDER: async (context) => {
    if (
      context.rootState.baskets.basket &&
      context.rootState.baskets.basket != null &&
      context.rootState.baskets.basket.length > 0
    ) {
      await ordersServices
        .createOrder({
          userId: context.rootState.users.user.userId,
          orderItems: context.rootState.baskets.basket,
        })
        .then(() => {
          context.dispatch("baskets/DELETE_BASKET", null, { root: true });
          context.dispatch("DELETE_SELECTED_ORDER_ITEMS");
        })
        .catch((e) => {
          console.log(e);
          context.dispatch("logs/LOG", e, { root: true });
        });
    }
  },
  DELETE_SELECTED_ORDER_ITEMS: async (context, payload) => {
    context.commit("CLEAR_SELECTED_ORDER_ITEM");
  },
  UPDATE_FILTER_ORDERS: async (context, payload) => {
    context.commit("EDIT_FILTER_ORDERS", payload);
  },
  DELETE_FILTER_ORDERS: async (context) => {
    context.commit("CLEAR_FILTER_ORDERS");
  },
};

const mutations = {
  SET_ORDERS: (state, payload) => {
    state.orders = payload;
  },
  SET_ORDER_ITEMS: (state, payload) => {
    state.orderItems = payload;
  },
  SET_ORDER_ITEMS_QUANTITIES: (state, payload) => {
    state.ordersItemsQuantities = payload;
  },
  EDIT_SELECTED_ORDER_ITEM: (state, payload) => {
    if (payload.quantity > 0) {
      Vue.set(
        state.selectedOrderItems,
        payload.lot + "." + payload.pointId,
        payload
      );
    } else {
      let orderItems = Object.assign({}, state.selectedOrderItems);

      delete orderItems[payload.lot + "." + payload.pointId];

      Vue.set(state, "selectedOrderItems", orderItems);
    }
  },
  CLEAR_SELECTED_ORDER_ITEM: (state) => {
    state.selectedOrderItems = {};
  },
  EDIT_FILTER_ORDERS: (state, payload) => {
    Vue.set(
      state.filterOrders,
      Object.keys(payload)[0],
      Object.values(payload)[0]
    );
  },
  CLEAR_FILTER_ORDERS: (state) => {
    state.filterOrders = {};
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
