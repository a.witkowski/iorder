﻿using AutoMapper;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Points.Queries.GetPoints
{
    public class GetPointsQueryHander : IRequestHandler<GetPointsQuery, IEnumerable<PointViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetPointsQueryHander> _logger;

        public GetPointsQueryHander(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetPointsQueryHander> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<PointViewModel>> Handle(GetPointsQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetPointsQueryHander Start");

            try
            {
                IList<Point> points = null;
                bool isUserPointsExists = await _context.UserPoints.AnyAsync(x => x.UserId == request.UserId, cancellationToken);
                if (isUserPointsExists)
                {
                    points = await _context.Points
                        .Include(x => x.Organization)
                        .Where(x => !x.Deleted && x.Organization.Users.Any(u => u.Id == request.UserId) && x.UserPoints.Any(u => u.UserId == request.UserId))
                        .OrderBy(x => x.Address)
                        .ToListAsync(cancellationToken);
                }
                else
                {
                    points = await _context.Points
                        .Include(x => x.Organization)
                        .Where(x => !x.Deleted && x.Organization.Users.Any(u => u.Id == request.UserId))
                        .OrderBy(x => x.Address)
                        .ToListAsync(cancellationToken);
                }

                var model = _mapper.Map<IEnumerable<PointViewModel>>(points);

                _logger.LogInformation("GetPointsQueryHander End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);

                throw;
            }
        }
    }
}
