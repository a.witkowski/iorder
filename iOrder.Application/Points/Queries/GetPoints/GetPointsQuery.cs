﻿using MediatR;
using System;
using System.Collections.Generic;

namespace iOrder.Application.Points.Queries.GetPoints
{
    public class GetPointsQuery : IRequest<IEnumerable<PointViewModel>>
    {
        public Guid UserId { get; set; }
    }
}
