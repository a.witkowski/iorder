﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;

namespace iOrder.Application.Points.Queries.GetPoints
{
    public class PointViewModel : IHaveCustomMapping
    {
        public Guid PointId { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Point, PointViewModel>()
                .ForMember(v => v.PointId, x => x.MapFrom(m => m.Id))
                .ForMember(v => v.Name, x => x.MapFrom(m => m.Organization != null ? m.Organization.Name : string.Empty));
        }
    }
}
