﻿using AutoMapper;
using iOrder.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Points.Queries.GetChildUsersPoints
{
    public class GetChildUsersPointsQueryHandler : IRequestHandler<GetChildUsersPointsQuery, IEnumerable<ChildUsersPointsViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetChildUsersPointsQueryHandler> _logger;

        public GetChildUsersPointsQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetChildUsersPointsQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<ChildUsersPointsViewModel>> Handle(GetChildUsersPointsQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetChildUsersPointsQueryHandler Start");

            try
            {
                var users = await _context.Users
                    .Include(x => x.UserPoints)
                    .Include("UserPoints.Point")
                    .Where(x => x.ParentId == request.UserId && x.UserPoints.Any(p => !p.Point.Deleted))
                    .ToListAsync(cancellationToken);

                var model = _mapper.Map<IEnumerable<ChildUsersPointsViewModel>>(users);

                _logger.LogInformation("GetChildUsersPointsQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
