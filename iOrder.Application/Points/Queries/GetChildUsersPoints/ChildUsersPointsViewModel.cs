﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;
using System.Linq;
using System.Collections.Generic;

namespace iOrder.Application.Points.Queries.GetChildUsersPoints
{
    public class ChildUsersPointsViewModel : IHaveCustomMapping
    {
        public Guid UserId { get; set; }

        public IList<UsersPointViewModel> Points { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<User, ChildUsersPointsViewModel>()
                .ForMember(v => v.UserId, x => x.MapFrom(m => m.Id))
                .ForMember(v => v.Points, x => x.MapFrom(m => m.UserPoints))
                ;
        }
    }

    public class UsersPointViewModel : IHaveCustomMapping
    {
        public Guid PointId { get; set; }

        public string Address { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<UserPoint, UsersPointViewModel>()
                .ForMember(v => v.PointId, x => x.MapFrom(m => m.Point.Id))
                .ForMember(v => v.Address, x => x.MapFrom(m => m.Point.Address))
                ;
        }
    }
}
