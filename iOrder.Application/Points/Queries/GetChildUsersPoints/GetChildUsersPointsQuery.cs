﻿using MediatR;
using System;
using System.Collections.Generic;

namespace iOrder.Application.Points.Queries.GetChildUsersPoints
{
    public class GetChildUsersPointsQuery : IRequest<IEnumerable<ChildUsersPointsViewModel>>
    {
        public Guid UserId { get; set; }
    }
}
