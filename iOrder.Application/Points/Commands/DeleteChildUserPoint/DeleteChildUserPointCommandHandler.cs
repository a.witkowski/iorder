﻿using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Points.Commands.DeleteChildUserPoint
{
    public class DeleteChildUserPointCommandHandler : IRequestHandler<DeleteChildUserPointCommand, Unit>
    {
        private readonly IOrderDbContext _context;
        private readonly ILogger<DeleteChildUserPointCommandHandler> _logger;

        public DeleteChildUserPointCommandHandler(
            IOrderDbContext context,
            ILogger<DeleteChildUserPointCommandHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Unit> Handle(DeleteChildUserPointCommand request, CancellationToken cancellationToken)
        {
            UserPoint userPoint = await _context.UserPoints.SingleOrDefaultAsync(x => x.UserId == request.UserId && x.PointId == request.PointId);
            if (userPoint == null)
            {
                _logger.LogInformation($"Point {request.PointId} for User {request.UserId} not found");
                throw new NotFoundException(nameof(UserPoint), $"{request.UserId} / {request.PointId}");
            }

            try
            {
                _context.UserPoints.Remove(userPoint);

                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }

            return Unit.Value;
        }
    }
}
