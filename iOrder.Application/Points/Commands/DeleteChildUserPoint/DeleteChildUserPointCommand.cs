﻿using MediatR;
using System;

namespace iOrder.Application.Points.Commands.DeleteChildUserPoint
{
    public class DeleteChildUserPointCommand : IRequest
    {
        public Guid UserId { get; set; }

        public Guid PointId { get; set; }
    }
}
