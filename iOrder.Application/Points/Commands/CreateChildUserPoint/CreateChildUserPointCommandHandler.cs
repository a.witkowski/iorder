﻿using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Points.Commands.CreateChildUserPoint
{
    public class CreateChildUserPointCommandHandler : IRequestHandler<CreateChildUserPointCommand, Unit>
    {
        private readonly IOrderDbContext _context;
        private readonly ILogger<CreateChildUserPointCommandHandler> _logger;

        public CreateChildUserPointCommandHandler(
            IOrderDbContext context,
            ILogger<CreateChildUserPointCommandHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Unit> Handle(CreateChildUserPointCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("CreateChildUserPointCommandHandler Start");

            bool isUserPointsExists = await _context.UserPoints.AnyAsync(x => x.UserId == request.UserId && x.PointId == request.PointId, cancellationToken);
            if (isUserPointsExists)
            {
                _logger.LogInformation($"Point {request.PointId} already exists");
                throw new Exception("Пункт уже существует");
            }

            try
            {
                _context.UserPoints.Add(new UserPoint { UserId = request.UserId, PointId = request.PointId });

                await _context.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }

            _logger.LogInformation("CreateChildUserPointCommandHandler End");

            return Unit.Value;
        }
    }
}
