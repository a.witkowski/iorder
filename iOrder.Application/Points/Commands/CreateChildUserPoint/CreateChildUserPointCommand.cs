﻿using MediatR;
using System;

namespace iOrder.Application.Points.Commands.CreateChildUserPoint
{
    public class CreateChildUserPointCommand : IRequest
    {
        public Guid UserId { get; set; }

        public Guid PointId { get; set; }
    }
}
