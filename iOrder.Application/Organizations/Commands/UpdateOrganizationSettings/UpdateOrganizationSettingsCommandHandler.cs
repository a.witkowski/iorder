﻿using AutoMapper;
using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Organizations.Commands.UpdateOrganizationSettings
{
    public class UpdateOrganizationSettingsCommandHandler : IRequestHandler<UpdateOrganizationSettingsCommand>
    {
        readonly IOrderDbContext _context;
        readonly IMapper _mapper;
        readonly ILogger<UpdateOrganizationSettingsCommandHandler> _logger;

        public UpdateOrganizationSettingsCommandHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<UpdateOrganizationSettingsCommandHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Unit> Handle(UpdateOrganizationSettingsCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("UpdateOrganizationSettingsCommandHandler Start");

            try
            {
                OrganizationSetting organizationSetting = await _context.OrganizationSettings
                               .SingleOrDefaultAsync(x => x.Id == request.OrganizationId);
                if (organizationSetting == null)
                {
                    _logger.LogInformation($"Organization {request.OrganizationId} not found");
                    throw new NotFoundException(nameof(Organization), request.OrganizationId);
                }

                var currentData = DateTime.Now;

                if (request.UsePharmacy.HasValue)
                    organizationSetting.UsePharmacy = request.UsePharmacy.Value;

                if (request.UseCosmetic.HasValue)
                    organizationSetting.UseCosmetic = request.UseCosmetic.Value;

                if (request.UseDietarySupplement.HasValue)
                    organizationSetting.UseDietarySupplement = request.UseDietarySupplement.Value;

                if (request.UseOptics.HasValue)
                    organizationSetting.UseOptics = request.UseOptics.Value;

                if (request.UseMedicalProducts.HasValue)
                    organizationSetting.UseMedicalProducts = request.UseMedicalProducts.Value;

                if (request.UseMedicalEquipment.HasValue)
                    organizationSetting.UseMedicalEquipment = request.UseMedicalEquipment.Value;

                organizationSetting.Updated = currentData;

                await _context.SaveChangesAsync(cancellationToken);

                _logger.LogInformation("UpdateOrganizationSettingsCommandHandler End");

                return Unit.Value;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
