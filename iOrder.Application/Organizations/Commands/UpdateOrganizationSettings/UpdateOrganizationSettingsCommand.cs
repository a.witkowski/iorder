﻿using MediatR;
using System;

namespace iOrder.Application.Organizations.Commands.UpdateOrganizationSettings
{
    public class UpdateOrganizationSettingsCommand : IRequest
    {
        public Guid OrganizationId { get; set; }

        public bool? UsePharmacy { get; set; }

        public bool? UseDietarySupplement { get; set; }

        public bool? UseOptics { get; set; }

        public bool? UseCosmetic { get; set; }

        public bool? UseMedicalEquipment { get; set; }

        public bool? UseMedicalProducts { get; set; }
    }
}
