﻿using AutoMapper;
using iOrder.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Organizations.Queries.GetOrganizationSettings
{
    public class GetOrganizationSettingsQueryHandler : IRequestHandler<GetOrganizationSettingsQuery, OrganizationSettingViewModel>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetOrganizationSettingsQueryHandler> _logger;

        public GetOrganizationSettingsQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetOrganizationSettingsQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<OrganizationSettingViewModel> Handle(GetOrganizationSettingsQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetOrganizationSettingsQueryHandler Start");

            try
            {
                var settings = await _context
                    .OrganizationSettings
                    .Include(x => x.Organization)
                        .ThenInclude(x => x.Contracts)
                    .Include("Organization.Contracts.ContractType")
                    .SingleOrDefaultAsync(x => x.Organization.Users.Any(u => u.Id == request.UserId));

                var model = _mapper.Map<OrganizationSettingViewModel>(settings);

                if (settings.Organization.Contracts.Count == 1)
                {
                    var contract = settings.Organization.Contracts.FirstOrDefault();
                    switch (contract.ContractType.Type.ToLower())
                    {
                        case "аптечный":
                            model.UseCosmetic = null;
                            break;
                        case "косметика":
                            model.UsePharmacy = model.UseDietarySupplement = model.UseOptics = model.UseMedicalEquipment = model.UseMedicalProducts = null;
                            break;
                    }
                }

                _logger.LogInformation("GetOrganizationSettingsQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
