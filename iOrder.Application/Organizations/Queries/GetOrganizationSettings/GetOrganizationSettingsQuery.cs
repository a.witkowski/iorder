﻿using MediatR;
using System;

namespace iOrder.Application.Organizations.Queries.GetOrganizationSettings
{
    public class GetOrganizationSettingsQuery : IRequest<OrganizationSettingViewModel>
    {
        public Guid UserId { get; set; }
    }
}
