﻿using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;

namespace iOrder.Application.Organizations.Queries.GetOrganizationSettings
{
    public class OrganizationSettingViewModel : IMapFrom<OrganizationSetting>
    {
        public int CreditDays { get; set; }

        #region Settings

        public bool? UsePharmacy { get; set; }

        public bool? UseDietarySupplement { get; set; }

        public bool? UseOptics { get; set; }

        public bool? UseCosmetic { get; set; }

        public bool? UseMedicalEquipment { get; set; }

        public bool? UseMedicalProducts { get; set; }

        #endregion

        #region Managers

        public string PharmacyManager { get; set; }

        public string OpticsManager { get; set; }

        public string CosmeticManager { get; set; }

        public string MedicalEquipmentManager { get; set; }

        #endregion

        #region Phones

        public string PharmacyManagerPhone { get; set; }

        public string OpticsManagerPhone { get; set; }

        public string CosmeticManagerPhone { get; set; }

        public string MedicalEquipmentManagerPhone { get; set; }

        #endregion

        #region Emails

        public string PharmacyManagerEmail { get; set; }

        public string OpticsManagerEmail { get; set; }

        public string CosmeticManagerEmail { get; set; }

        public string MedicalEquipmentManagerEmail { get; set; }

        #endregion

        #region Skypes

        public string PharmacyManagerSkype { get; set; }

        public string OpticsManagerSkype { get; set; }

        public string CosmeticManagerSkype { get; set; }

        public string MedicalEquipmentManagerSkype { get; set; }

        #endregion
    }
}
