﻿using AutoMapper;
using iOrder.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Organizations.Queries.GetOrganizationsSettings
{
    public class GetOrganizationsSettingsQueryHandler : IRequestHandler<GetOrganizationsSettingsQuery, IEnumerable<OrganizationsSettingViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetOrganizationsSettingsQueryHandler> _logger;

        public GetOrganizationsSettingsQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetOrganizationsSettingsQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<OrganizationsSettingViewModel>> Handle(GetOrganizationsSettingsQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetOrganizationsSettingsQueryHandler Start");

            try
            {
                var settings = await _context
                    .OrganizationSettings
                    .Include(x => x.Organization)
                        .ThenInclude(x => x.OrganizationType)
                    .OrderBy(x => x.Organization.Name)
                    .ToListAsync();

                var model = _mapper.Map<IEnumerable<OrganizationsSettingViewModel>>(settings);

                _logger.LogInformation("GetOrganizationsSettingsQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
