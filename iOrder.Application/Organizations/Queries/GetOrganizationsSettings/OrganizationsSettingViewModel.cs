﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;

namespace iOrder.Application.Organizations.Queries.GetOrganizationsSettings
{
    public class OrganizationsSettingViewModel : IHaveCustomMapping
    {
        public Guid OrganizationId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        #region Settings

        public bool UsePharmacy { get; set; }

        public bool UseDietarySupplement { get; set; }

        public bool UseOptics { get; set; }

        public bool UseCosmetic { get; set; }

        public bool UseMedicalEquipment { get; set; }

        public bool UseMedicalProducts { get; set; }

        #endregion

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<OrganizationSetting, OrganizationsSettingViewModel>()
                .ForMember(v => v.OrganizationId, x => x.MapFrom(m => m.Id))
                .ForMember(v => v.Name, x => x.MapFrom(m => m.Organization.Name))
                .ForMember(v => v.Type, x => x.MapFrom(m => m.Organization.OrganizationType.Type))
                ;
        }
    }
}
