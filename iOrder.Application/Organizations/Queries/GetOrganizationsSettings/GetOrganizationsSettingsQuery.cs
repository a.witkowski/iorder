﻿using MediatR;
using System.Collections.Generic;

namespace iOrder.Application.Organizations.Queries.GetOrganizationsSettings
{
    public class GetOrganizationsSettingsQuery : IRequest<IEnumerable<OrganizationsSettingViewModel>>
    {
    }
}
