﻿using MediatR;
using System;
using System.Collections.Generic;

namespace iOrder.Application.Organizations.Queries.GetOrganizationUsers
{
    public class GetOrganizationUsersQuery : IRequest<IEnumerable<OrganizationUserViewModel>>
    {
        public Guid OrganizationId { get; set; }
    }
}
