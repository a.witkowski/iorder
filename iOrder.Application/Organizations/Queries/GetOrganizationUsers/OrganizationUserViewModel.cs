﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;

namespace iOrder.Application.Organizations.Queries.GetOrganizationUsers
{
    public class OrganizationUserViewModel : IHaveCustomMapping
    {
        public Guid UserId { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        #region OrganizationSettings

        public bool OrganizationUsePharmacy { get; set; }

        public bool OrganizationUseDietarySupplement { get; set; }

        public bool OrganizationUseOptics { get; set; }

        public bool OrganizationUseCosmetic { get; set; }

        public bool OrganizationUseMedicalEquipment { get; set; }

        public bool OrganizationUseMedicalProducts { get; set; }

        #endregion

        public UserSettingViewModel Settings { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<User, OrganizationUserViewModel>()
                .ForMember(v => v.UserId, x => x.MapFrom(m => m.Id))
                .ForMember(v => v.OrganizationUsePharmacy, x => x.MapFrom(m => m.Organization.OrganizationSetting.UsePharmacy))
                .ForMember(v => v.OrganizationUseDietarySupplement, x => x.MapFrom(m => m.Organization.OrganizationSetting.UseDietarySupplement))
                .ForMember(v => v.OrganizationUseOptics, x => x.MapFrom(m => m.Organization.OrganizationSetting.UseOptics))
                .ForMember(v => v.OrganizationUseCosmetic, x => x.MapFrom(m => m.Organization.OrganizationSetting.UseCosmetic))
                .ForMember(v => v.OrganizationUseMedicalEquipment, x => x.MapFrom(m => m.Organization.OrganizationSetting.UseMedicalEquipment))
                .ForMember(v => v.OrganizationUseMedicalProducts, x => x.MapFrom(m => m.Organization.OrganizationSetting.UseMedicalProducts))
                .ForMember(v => v.Settings, x => x.MapFrom(m => m.UserSetting))
                ;
        }
    }

    public class UserSettingViewModel : IMapFrom<UserSetting>
    {
        public Guid Id { get; set; }

        #region Settings

        public bool UsePharmacy { get; set; }

        public bool UseDietarySupplement { get; set; }

        public bool UseOptics { get; set; }

        public bool UseCosmetic { get; set; }

        public bool UseMedicalEquipment { get; set; }

        public bool UseMedicalProducts { get; set; }

        #endregion
    }
}
