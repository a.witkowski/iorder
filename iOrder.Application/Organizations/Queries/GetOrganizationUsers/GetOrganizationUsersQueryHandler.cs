﻿using AutoMapper;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Organizations.Queries.GetOrganizationUsers
{
    public class GetOrganizationUsersQueryHandler : IRequestHandler<GetOrganizationUsersQuery, IEnumerable<OrganizationUserViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetOrganizationUsersQueryHandler> _logger;

        public GetOrganizationUsersQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetOrganizationUsersQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<OrganizationUserViewModel>> Handle(GetOrganizationUsersQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetOrganizationUsersQueryHandler Start");

            try
            {
                IList<User> users = await _context.Users
                    .Include(x => x.UserSetting)
                    .Include(x => x.Organization)
                        .ThenInclude(x => x.OrganizationSetting)
                    .Where(x => x.OrganizationId == request.OrganizationId && !x.Deleted)
                    .ToListAsync(cancellationToken);

                var model = _mapper.Map<IEnumerable<OrganizationUserViewModel>>(users);

                _logger.LogInformation("GetOrganizationUsersQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
