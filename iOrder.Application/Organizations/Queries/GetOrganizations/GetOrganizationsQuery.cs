﻿using MediatR;
using System.Collections.Generic;

namespace iOrder.Application.Organizations.Queries.GetOrganizations
{
    public class GetOrganizationsQuery : IRequest<IEnumerable<OrganizationViewModel>>
    {
    }
}
