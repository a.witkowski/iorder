﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;

namespace iOrder.Application.Organizations.Queries.GetOrganizations
{
    public class OrganizationViewModel : IHaveCustomMapping
    {
        public Guid Id { get; set; }

        public int OrganizationTypeId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Organization, OrganizationViewModel>()
                .ForMember(v => v.Type, x => x.MapFrom(m => m.OrganizationType.Type))
                ;
        }
    }
}
