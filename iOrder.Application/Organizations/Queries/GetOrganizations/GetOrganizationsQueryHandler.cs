﻿using AutoMapper;
using iOrder.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Organizations.Queries.GetOrganizations
{
    public class GetOrganizationsQueryHandler : IRequestHandler<GetOrganizationsQuery, IEnumerable<OrganizationViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetOrganizationsQueryHandler> _logger;

        public GetOrganizationsQueryHandler(IOrderDbContext context,
            IMapper mapper,
            ILogger<GetOrganizationsQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<OrganizationViewModel>> Handle(GetOrganizationsQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetOrganizationsQueryHandler Start");

            try
            {
                var organizations = await _context.Organizations
                    .Include(x => x.OrganizationType)
                    .ToListAsync();

                var model = _mapper.Map<IEnumerable<OrganizationViewModel>>(organizations);

                _logger.LogInformation("GetOrganizationsQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
