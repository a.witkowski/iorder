﻿using MediatR;
using System;
using System.Collections.Generic;

namespace iOrder.Application.Organizations.Queries.GetContracts
{
    public class GetContractsQuery : IRequest<IEnumerable<ContractViewModel>>
    {
        public Guid UserId { get; set; }
    }
}
