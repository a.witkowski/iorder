﻿using AutoMapper;
using iOrder.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Organizations.Queries.GetContracts
{
    public class GetContractsQueryHandler : IRequestHandler<GetContractsQuery, IEnumerable<ContractViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetContractsQueryHandler> _logger;

        public GetContractsQueryHandler(IOrderDbContext context,
            IMapper mapper,
            ILogger<GetContractsQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<ContractViewModel>> Handle(GetContractsQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetContractsQueryHandler Start");

            try
            {
                var contracts = await _context.Contracts
                    .Include(x => x.ContractType)
                    .Where(x => x.Organization.Users.Any(u => u.Id == request.UserId))
                    .ToListAsync();

                var model = _mapper.Map<IEnumerable<ContractViewModel>>(contracts);

                _logger.LogInformation("GetContractsQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
