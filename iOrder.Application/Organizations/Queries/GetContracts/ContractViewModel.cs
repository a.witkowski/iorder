﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;

namespace iOrder.Application.Organizations.Queries.GetContracts
{
    public class ContractViewModel : IHaveCustomMapping
    {
        public string ContractType { get; set; }

        public string Name { get; set; }

        public DateTime Start { get; set; }

        public DateTime? End { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Contract, ContractViewModel>()
                .ForMember(v => v.ContractType, x => x.MapFrom(m => m.ContractType != null ? m.ContractType.Type : string.Empty))
                ;
        }
    }
}
