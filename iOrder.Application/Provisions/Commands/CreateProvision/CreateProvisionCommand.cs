﻿using MediatR;
using System;

namespace iOrder.Application.Provisions.Commands.CreateProvision
{
    public class CreateProvisionCommand : IRequest<Guid>
    {
        public Guid UserId { get; set; }

        public Guid? PointId { get; set; }

        public Guid ManufacturerId { get; set; }

        public Guid? ProductId { get; set; }

        public double? Price { get; set; }

        public int? Quantity { get; set; }

        public DateTime? Start { get; set; }

        public DateTime End { get; set; }
    }
}
