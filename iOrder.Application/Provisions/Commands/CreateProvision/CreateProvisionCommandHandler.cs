﻿using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Provisions.Commands.CreateProvision
{
    public class CreateProvisionCommandHandler : IRequestHandler<CreateProvisionCommand, Guid>
    {
        private readonly IOrderDbContext _context;
        private readonly ILogger<CreateProvisionCommandHandler> _logger;

        public CreateProvisionCommandHandler(
            IOrderDbContext context,
            ILogger<CreateProvisionCommandHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Guid> Handle(CreateProvisionCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("CreateProvisionCommandHandler Start");

            User user = await _context.Users
                .Include(x => x.Organization)
                .SingleOrDefaultAsync(x => x.Id == request.UserId);
            if (user == null)
            {
                _logger.LogInformation($"User {request.UserId} not found");
                throw new NotFoundException(nameof(User), request.UserId);
            }

            try
            {
                var currentData = DateTime.Now;

                var newProvision = new Provision()
                {
                    OrganizationId = user.OrganizationId.Value,
                    PointId = request.PointId,
                    ManufacturerId = request.ManufacturerId,
                    ProductId = request.ProductId,
                    Price = request.Price,
                    Quantity = request.Quantity,
                    Start = request.Start ?? currentData.Date,
                    End = request.End,
                    Created = currentData,
                    Updated = currentData
                };

                _context.Provisions.Add(newProvision);

                await _context.SaveChangesAsync(cancellationToken);

                _logger.LogInformation("CreateProvisionCommandHandler End");

                return newProvision.Id;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
