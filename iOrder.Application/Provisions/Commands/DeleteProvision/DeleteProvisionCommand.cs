﻿using MediatR;
using System;

namespace iOrder.Application.Provisions.Commands.DeleteProvision
{
    public class DeleteProvisionCommand : IRequest
    {
        public Guid ProvisionId { get; set; }
    }
}
