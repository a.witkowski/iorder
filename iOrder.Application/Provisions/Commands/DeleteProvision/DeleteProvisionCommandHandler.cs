﻿using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Provisions.Commands.DeleteProvision
{
    public class DeleteProvisionCommandHandler : IRequestHandler<DeleteProvisionCommand, Unit>
    {
        private readonly IOrderDbContext _context;
        private readonly ILogger<DeleteProvisionCommandHandler> _logger;

        public DeleteProvisionCommandHandler(
            IOrderDbContext context,
            ILogger<DeleteProvisionCommandHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Unit> Handle(DeleteProvisionCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("DeleteProvisionCommandHandler Start");

            var entity = await _context.Provisions.FindAsync(request.ProvisionId);
            if (entity == null)
            {
                _logger.LogInformation($"Provision {request.ProvisionId} not found");
                throw new NotFoundException(nameof(Provision), request.ProvisionId);
            }

            try
            {
                _context.Provisions.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                _logger.LogInformation("DeleteProvisionCommandHandler End");

                return Unit.Value;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
