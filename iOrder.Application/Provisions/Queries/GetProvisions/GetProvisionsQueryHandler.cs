﻿using AutoMapper;
using iOrder.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Provisions.Queries.GetProvisions
{
    public class GetProvisionsQueryHandler : IRequestHandler<GetProvisionsQuery, IEnumerable<ProvisionViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetProvisionsQueryHandler> _logger;

        public GetProvisionsQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetProvisionsQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<ProvisionViewModel>> Handle(GetProvisionsQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetProvisionsQueryHandler Start");

            try
            {
                var provisions = await _context.Provisions
                        .Where(x => x.End.Date >= DateTime.Today && x.Organization.Users.Any(u => u.Id == request.UserId))
                        .Include(x => x.Manufacturer)
                        .Include(x => x.Product)
                        .Include(x => x.Point)
                        .OrderByDescending(x => x.Created)
                        .ToListAsync(cancellationToken);

                var model = _mapper.Map<IEnumerable<ProvisionViewModel>>(provisions);

                _logger.LogInformation("GetProvisionsQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
