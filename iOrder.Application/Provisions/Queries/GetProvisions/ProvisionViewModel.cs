﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;

namespace iOrder.Application.Provisions.Queries.GetProvisions
{
    public class ProvisionViewModel : IHaveCustomMapping
    {
        public Guid ProvisionId { get; set; }

        public string PointAddress { get; set; }

        public string Manufacturer { get; set; }

        public string ProductName { get; set; }

        public double? Price { get; set; }

        public int? Quantity { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Provision, ProvisionViewModel>()
                .ForMember(v => v.ProvisionId, x => x.MapFrom(m => m.Id))
                .ForMember(v => v.PointAddress, x => x.MapFrom(m => m.Point.Address))
                .ForMember(v => v.Manufacturer, x => x.MapFrom(m => m.Manufacturer.Name))
                .ForMember(v => v.ProductName, x => x.MapFrom(m => m.Product.Name))
                ;
        }
    }
}
