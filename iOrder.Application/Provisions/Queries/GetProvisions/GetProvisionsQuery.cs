﻿using MediatR;
using System;
using System.Collections.Generic;

namespace iOrder.Application.Provisions.Queries.GetProvisions
{
    public class GetProvisionsQuery : IRequest<IEnumerable<ProvisionViewModel>>
    {
        public Guid UserId { get; set; }

        public Guid? PointId { get; set; }
    }
}
