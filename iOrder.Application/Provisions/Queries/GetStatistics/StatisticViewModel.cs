﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;

namespace iOrder.Application.Provisions.Queries.GetStatistics
{
    public class StatisticViewModel : IHaveCustomMapping
    {
        public string PointAddress { get; set; }

        public string Manufacturer { get; set; }

        public string ProductName { get; set; }

        public double? ProvisionPrice { get; set; }

        public int? ProvisionQuantity { get; set; }

        public double? OrderPrice { get; set; }

        public int? OrderQuantity { get; set; }

        public double? ProgressPrice { get; set; }

        public double? ProgressQuantity { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Provision, StatisticViewModel>()
                .ForMember(v => v.Manufacturer, x => x.MapFrom(m => m.Manufacturer.Name))
                ;
        }
    }
}
