﻿using MediatR;
using System;
using System.Collections.Generic;

namespace iOrder.Application.Provisions.Queries.GetStatistics
{
    public class GetStatisticsQuery : IRequest<IEnumerable<StatisticViewModel>>
    {
        public Guid UserId { get; set; }

        public DateTime? Start { get; set; }

        public DateTime? End { get; set; }
    }
}
