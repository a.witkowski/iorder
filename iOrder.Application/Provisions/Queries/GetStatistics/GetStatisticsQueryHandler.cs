﻿using AutoMapper;
using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Provisions.Queries.GetStatistics
{
    public class GetStatisticsQueryHandler : IRequestHandler<GetStatisticsQuery, IEnumerable<StatisticViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetStatisticsQueryHandler> _logger;

        public GetStatisticsQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetStatisticsQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<StatisticViewModel>> Handle(GetStatisticsQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetStatisticsQueryHandler Start");

            User user = await _context.Users
                .Include(x => x.UserRoles)
                .Include("UserRoles.Role")
                .SingleOrDefaultAsync(x => x.Id == request.UserId && !x.Deleted);
            if (user == null)
            {
                _logger.LogInformation($"User {request.UserId} not found");
                throw new NotFoundException(nameof(User), request.UserId);
            }

            try
            {
                var queryProvisions = _context.Provisions
                    .Where(x => x.Organization.Users.Any(u => u.Id == request.UserId) && x.End >= DateTime.Today);

                var queryOrderItems = _context.OrderItems
                    .Include(x => x.Order)
                    .Where(x => x.Order.User.Organization.Users.Any(u => u.Id == request.UserId && x.OrderItemState.State.ToLower() == "доставлен"));

                if (!user.UserRoles.Any(x => x.Role.Name.Equals("manager", StringComparison.OrdinalIgnoreCase)))
                {
                    queryProvisions = queryProvisions.Where(x => x.Organization.Users.FirstOrDefault(u => u.Id == request.UserId).UserPoints.Any(p => p.UserId == request.UserId));
                    queryOrderItems = queryOrderItems.Where(x => x.Order.Point.UserPoints.Any(p => p.UserId == request.UserId));
                }

                if (request.Start.HasValue)
                {
                    queryOrderItems = queryOrderItems.Where(x => x.Order.Created.Date >= request.Start.Value.Date);
                }
                else
                {
                    queryOrderItems = queryOrderItems.Where(x => x.Order.Created.Date <= DateTime.Today);
                }

                if (request.End.HasValue)
                {
                    queryOrderItems = queryOrderItems.Where(x => x.Order.Created.Date <= request.End.Value.Date);
                }
                else
                {
                    queryOrderItems = queryOrderItems.Where(x => x.Order.Created.Date <= DateTime.Today);
                }

                var orderItems = await queryOrderItems
                    .Include(x => x.Order)
                    .Include(x => x.Product)
                        .ThenInclude(x => x.Factory)
                    .ToListAsync(cancellationToken);

                var provisions = await queryProvisions
                    .Include(x => x.Manufacturer)
                    .Include(x => x.Product)
                    .Include(x => x.Point)
                    .OrderBy(x => x.Manufacturer.Name)
                        .ThenBy(x => x.Product.Name)
                    .ToListAsync(cancellationToken);

                var tempProvisions = provisions.GroupBy(x => new { x.PointId, Manufacturer = x.Manufacturer.Name, Product = x.Product?.Name, x.Start, x.End });

                var result = tempProvisions
                    .Select(x => new StatisticViewModel
                    {
                        PointAddress = x.Key.PointId != null ? x.FirstOrDefault(p => p.Point.Id == x.Key.PointId).Point.Address : null,
                        Start = x.Key.Start,
                        End = x.Key.End,
                        Manufacturer = x.Key.Manufacturer,
                        ProductName = x.Key.Product,
                        ProvisionPrice = x.Sum(y => y.Price),
                        ProvisionQuantity = x.Sum(y => y.Quantity),
                        OrderPrice = !string.IsNullOrWhiteSpace(x.Key.Product)
                                     ? orderItems.Where(o =>
                                        o.Product.Id == x.FirstOrDefault(p => p.ProductId == o.Product.Id)?.ProductId
                                        && (x.Key.PointId != null ? o.Order.PointId == x.Key.PointId : true)
                                        && (request.Start.HasValue
                                            ? o.Order.Created.Date >= request.Start.Value
                                            : o.Order.Created.Date >= x.Min(p => p.Start)))?
                                      .Sum(i => i.Price * i.Quantity)
                                     : orderItems.Where(o =>
                                        o.Product.Factory.ManufacturerId == x.FirstOrDefault(p => p.ManufacturerId == o.Product.Factory.ManufacturerId)?.ManufacturerId
                                        && (x.Key.PointId != null ? o.Order.PointId == x.Key.PointId : true)
                                        && (request.Start.HasValue
                                            ? o.Order.Created.Date >= request.Start.Value
                                            : o.Order.Created.Date >= x.Min(p => p.Start)))?
                                      .Sum(i => i.Price * i.Quantity),
                        OrderQuantity = !string.IsNullOrWhiteSpace(x.Key.Product)
                                     ? orderItems.Where(o =>
                                        o.Product.Id == x.FirstOrDefault(p => p.ProductId == o.Product.Id)?.ProductId
                                        && (x.Key.PointId != null ? o.Order.PointId == x.Key.PointId : true)
                                        && (request.Start.HasValue
                                            ? o.Order.Created.Date >= request.Start.Value
                                            : o.Order.Created.Date >= x.Min(p => p.Start)))?
                                      .Sum(i => i.Quantity)
                                     : orderItems.Where(o =>
                                        o.Product.Factory.ManufacturerId == x.FirstOrDefault(p => p.ManufacturerId == o.Product.Factory.ManufacturerId)?.ManufacturerId
                                        && (x.Key.PointId != null ? o.Order.PointId == x.Key.PointId : true)
                                        && (request.Start.HasValue
                                            ? o.Order.Created.Date >= request.Start.Value
                                            : o.Order.Created.Date >= x.Min(p => p.Start)))?
                                      .Sum(i => i.Quantity),
                    }).ToList();

                result.All(x =>
                {
                    x.ProvisionPrice = x.ProvisionPrice > 0 ? x.ProvisionPrice : null;
                    x.ProvisionQuantity = x.ProvisionQuantity > 0 ? x.ProvisionQuantity : null;
                    x.OrderPrice = x.ProvisionPrice > 0 ? x.OrderPrice : null;
                    x.OrderQuantity = x.ProvisionQuantity > 0 ? x.OrderQuantity : null;
                    x.ProgressPrice = x.ProvisionPrice > 0 ? Math.Round(x.OrderPrice.Value / x.ProvisionPrice.Value * 100, 2) : 0;
                    x.ProgressQuantity = x.ProvisionQuantity > 0 ? Math.Round((double)x.OrderQuantity.Value / (double)x.ProvisionQuantity.Value * 100, 2) : 0;
                    return true;
                });

                _logger.LogInformation("GetStatisticsQueryHandler End");

                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
