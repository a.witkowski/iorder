﻿using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;

namespace iOrder.Application.Orders.Commands.CreateOrder
{
    public class CreateOrderItemCommand : IMapFrom<OrderItem>
    {
        public Guid ProductId { get; set; }

        public Guid PointId { get; set; }

        public double Price { get; set; }

        public int Quantity { get; set; }

        public DateTime? Expiration { get; set; }
    }
}
