﻿using MediatR;
using System;
using System.Collections.Generic;

namespace iOrder.Application.Orders.Commands.CreateOrder
{
    public class CreateOrderCommand : IRequest<Guid>
    {
        public Guid UserId { get; set; }

        public IList<CreateOrderItemCommand> OrderItems { get; set; }
    }
}
