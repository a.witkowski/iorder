﻿using AutoMapper;
using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Orders.Commands.CreateOrder
{
    public class CreateOrderCommandHandler : IRequestHandler<CreateOrderCommand, Guid>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<CreateOrderCommandHandler> _logger;
        private readonly IFTPServices _fTPServices;

        public CreateOrderCommandHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<CreateOrderCommandHandler> logger,
            IFTPServices fTPServices)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _fTPServices = fTPServices ?? throw new ArgumentNullException(nameof(fTPServices));
        }

        public async Task<Guid> Handle(CreateOrderCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("CreateOrderCommandHandler Start");

            if (request == null || request.OrderItems == null || request.OrderItems.Count <= 0)
            {
                _logger.LogError("CreateOrderCommandHandler: OrderItems is empty");
                throw new Exception("CreateOrderCommandHandler: OrderItems is empty");
            }

            _logger.LogInformation($"CreateOrderCommandHandler: UserId={request.UserId}");

            foreach (var item in request.OrderItems)
            {
                _logger.LogInformation($"CreateOrderCommandHandler: PointId={item.PointId}" +
                    $", ProductId={item.ProductId}" +
                    $", Expiration={item.Expiration}" +
                    $", Price={item.Price}" +
                    $", Quantity={item.Quantity}");
            }

            User user = await _context.Users
                .Include(x => x.Organization)
                .Include(x => x.Organization.Points)
                .SingleOrDefaultAsync(x => x.Id == request.UserId);
            if (user == null)
            {
                _logger.LogError($"User {request.UserId} not found");
                throw new NotFoundException(nameof(User), request.UserId);
            }

            var currentData = DateTime.Now;

            try
            {
                IEnumerable<CreateOrderItemCommand> orderItems = request.OrderItems.Where(x => x.Quantity > 0);

                IList<Product> products = await _context.Products.Where(x => orderItems.Select(i => i.ProductId).Contains(x.Id))
                    .Distinct()
                    .ToListAsync();

                var orderItemGroupPoint = orderItems.GroupBy(x => x.PointId);

                foreach (var orderItemPoints in orderItemGroupPoint)
                {
                    StringBuilder file = new StringBuilder(100);

                    var order = new Order()
                    {
                        Number = $"{DateTime.Today.DayOfYear}{DateTime.Now:yy}-{DateTime.Now:HHmmssffff}",
                        UserId = request.UserId,
                        PointId = orderItemPoints.Key,
                        Created = currentData
                    };

                    foreach (var item in orderItemPoints)
                    {
                        Product product = products.Single(x => x.Id == item.ProductId);

                        OrderItem orderItem = new OrderItem
                        {
                            ProductId = item.ProductId,
                            Quantity = item.Quantity,
                            Expiration = item.Expiration,
                            Price = item.Price,
                            Created = currentData,
                            Updated = currentData
                        };

                        Point point = user.Organization.Points.Single(x => x.Id == order.PointId);

                        file.AppendLine($"{point.Id1C};" +
                            $"{user.Organization.Id1C}{currentData:ddMMyyyyHHmmss};" +
                            $"{currentData:dd.MM.yyyy};" +
                            $"{product.Id1C};" +
                            $"{orderItem.Expiration?.ToString("MM-yyyy")};" +
                            $"{orderItem.Price};" +
                            $"{orderItem.Quantity}");

                        _logger.LogInformation($"orderItem:" +
                            $" PointId={order.PointId}," +
                            $" ProductId={orderItem.ProductId}," +
                            $" Quantity={orderItem.Quantity}," +
                            $" Lot={orderItem.Lot}," +
                            $" Expiration={orderItem.Expiration}," +
                            $" Price={orderItem.Price}");

                        order.OrderItems.Add(orderItem);
                    }

                    _context.Orders.Add(order);

                    await _fTPServices.UploadAsync($"{order.Number}.eord", Encoding.UTF8.GetBytes(file.ToString()));
                }

                await _context.SaveChangesAsync(cancellationToken);

                _logger.LogInformation("CreateOrderCommandHandler End");
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, ex.Message, ex.StackTrace);
                throw new Exception(ex.Message, ex.InnerException);
            }

            return Guid.Empty;
        }
    }
}
