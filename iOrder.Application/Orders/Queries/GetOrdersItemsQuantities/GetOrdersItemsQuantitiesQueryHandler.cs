﻿using AutoMapper;
using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Orders.Queries.GetOrdersItemsQuantities
{
    public class GetOrdersItemsQuantitiesQueryHandler : IRequestHandler<GetOrdersItemsQuantitiesQuery, IEnumerable<OrdersItemsQuantitiesModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetOrdersItemsQuantitiesQueryHandler> _logger;

        public GetOrdersItemsQuantitiesQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetOrdersItemsQuantitiesQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<OrdersItemsQuantitiesModel>> Handle(GetOrdersItemsQuantitiesQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetOrdersItemsQuantitiesQueryHandler Start");

            User user = await _context.Users.FindAsync(request.UserId);
            if (user == null)
            {
                _logger.LogInformation($"User {request.UserId} not found");
                throw new NotFoundException(nameof(User), request.UserId);
            }

            DateTime date = new DateTime(request.Year.HasValue ? request.Year.Value : DateTime.Today.Year
                , request.Month.HasValue ? request.Month.Value : DateTime.Today.Month
                , DateTime.Today.Day);

            DateTime startDate = new DateTime(date.Year, date.Month, 1);

            DateTime endDate = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));

            try
            {
                var orders = _context.Orders
                    .Where(x => x.UserId == request.UserId && x.Created >= startDate && x.Created <= endDate);

                var data = await orders
                    .GroupBy(x => x.Created.Date)
                    .Select(r => new OrdersItemsQuantitiesModel
                    {
                        Date = r.Key,
                        Quantity = r.Sum(s => s.OrderItems.Sum(i => i.Quantity))
                    }).ToListAsync(cancellationToken);

                var model = _mapper.Map<IEnumerable<OrdersItemsQuantitiesModel>>(data);

                _logger.LogInformation("GetOrdersItemsQuantitiesQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
