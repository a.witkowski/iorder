﻿using MediatR;
using System;
using System.Collections.Generic;

namespace iOrder.Application.Orders.Queries.GetOrdersItemsQuantities
{
    public class GetOrdersItemsQuantitiesQuery : IRequest<IEnumerable<OrdersItemsQuantitiesModel>>
    {
        public Guid UserId { get; set; }

        public int? Month { get; set; }

        public int? Year { get; set; }
    }
}
