﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using System;

namespace iOrder.Application.Orders.Queries.GetOrdersItemsQuantities
{
    public class OrdersItemsQuantitiesModel : IHaveCustomMapping
    {
        public DateTime Date { get; set; }
        public int Quantity { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<OrdersItemsQuantitiesModel, OrdersItemsQuantitiesModel>()
                .ForMember(v => v.Date, x => x.MapFrom(m => m.Date))
                .ForMember(v => v.Quantity, x => x.MapFrom(m => m.Quantity))
                ;
        }
    }
}
