﻿using AutoMapper;
using iOrder.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Orders.Queries.GetOrderItems
{
    public class GetOrderItemsQueryHandler : IRequestHandler<GetOrderItemsQuery, IEnumerable<OrderItemViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetOrderItemsQueryHandler> _logger;

        public GetOrderItemsQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetOrderItemsQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<OrderItemViewModel>> Handle(GetOrderItemsQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetOrderItemsQueryHandler Start");

            try
            {
                var orderItems = await _context.OrderItems
                    .Include(x => x.Order)
                        .ThenInclude(x => x.Point)
                    .Include(x => x.OrderItemState)
                    .Include(x => x.Product)
                        .ThenInclude(x => x.Factory)
                            .ThenInclude(x => x.Manufacturer)
                    .Where(x => x.OrderId == request.OrderId && x.Order.User.Organization.Users.Any(u => u.Id == request.UserId))
                    .OrderBy(x => x.Product.Name)
                    .ToListAsync(cancellationToken);

                var model = _mapper.Map<IEnumerable<OrderItemViewModel>>(orderItems);

                _logger.LogInformation("GetOrderItemsQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
