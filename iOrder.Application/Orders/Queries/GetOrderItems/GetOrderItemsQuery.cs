﻿using MediatR;
using System;
using System.Collections.Generic;

namespace iOrder.Application.Orders.Queries.GetOrderItems
{
    public class GetOrderItemsQuery : IRequest<IEnumerable<OrderItemViewModel>>
    {
        public Guid UserId { get; set; }

        public Guid OrderId { get; set; }
    }
}
