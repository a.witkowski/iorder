﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;

namespace iOrder.Application.Orders.Queries.GetOrderItems
{
    public class OrderItemViewModel : IHaveCustomMapping
    {
        public Guid OrderItemId { get; set; }

        public Guid PointId { get; set; }

        public string ProductName { get; set; }

        public string Manufacturer { get; set; }

        public string Factory { get; set; }

        public string ProductCountry { get; set; }

        public string PointAddress { get; set; }

        public DateTime Expiration { get; set; }

        public double Price { get; set; }

        public int Quantity { get; set; }

        public string State { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<OrderItem, OrderItemViewModel>()
                .ForMember(v => v.OrderItemId, x => x.MapFrom(m => m.Id))
                .ForMember(v => v.ProductName, x => x.MapFrom(m => m.Product != null ? m.Product.Name : string.Empty))
                .ForMember(v => v.Manufacturer, x => x.MapFrom(m => m.Product != null && m.Product.Factory != null && m.Product.Factory.Manufacturer != null
                    ? m.Product.Factory.Manufacturer.Name
                    : string.Empty))
                .ForMember(v => v.Factory, x => x.MapFrom(m => m.Product != null && m.Product.Factory != null ? m.Product.Factory.Name : string.Empty))
                .ForMember(v => v.ProductCountry, x => x.MapFrom(m => m.Product != null && m.Product.Factory != null ? m.Product.Factory.Country : string.Empty))
                .ForMember(v => v.PointAddress, x => x.MapFrom(m => m.Order.Point != null ? m.Order.Point.Address : string.Empty))
                .ForMember(v => v.State, x => x.MapFrom(m => m.OrderItemState != null ? m.OrderItemState.State : string.Empty))
                ;
        }
    }
}
