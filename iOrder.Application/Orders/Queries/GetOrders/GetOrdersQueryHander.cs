﻿using AutoMapper;
using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Orders.Queries.GetOrders
{
    public class GetOrdersQueryHander : IRequestHandler<GetOrdersQuery, IEnumerable<OrderViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetOrdersQueryHander> _logger;

        public GetOrdersQueryHander(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetOrdersQueryHander> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<OrderViewModel>> Handle(GetOrdersQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetOrdersQueryHander Start");

            User user = await _context.Users
                .Include(x => x.UserRoles)
                .Include("UserRoles.Role")
                .SingleOrDefaultAsync(x => x.Id == request.UserId && !x.Deleted);
            if (user == null)
            {
                _logger.LogInformation($"User {request.UserId} not found");
                throw new NotFoundException(nameof(User), request.UserId);
            }

            try
            {
                var orders = _context.Orders
                    .Include(x => x.Point)
                    .Include(x => x.OrderItems)
                    .Include(x => x.User)
                    .Where(x => x.User.Organization.Users.Any(u => u.Id == request.UserId));

                if (!user.UserRoles.Any(x => x.Role.Name.Equals("manager", StringComparison.OrdinalIgnoreCase)))
                {
                    orders = orders.Where(x => x.UserId == user.Id);
                }

                IList<Order> result = await orders.OrderByDescending(x => x.Created).ToListAsync(cancellationToken);

                var model = _mapper.Map<IEnumerable<OrderViewModel>>(result);

                _logger.LogInformation("GetOrdersQueryHander End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
