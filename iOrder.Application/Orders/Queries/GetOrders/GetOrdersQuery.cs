﻿using MediatR;
using System;
using System.Collections.Generic;

namespace iOrder.Application.Orders.Queries.GetOrders
{
    public class GetOrdersQuery : IRequest<IEnumerable<OrderViewModel>>
    {
        public Guid UserId { get; set; }
    }
}
