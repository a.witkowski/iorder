﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;
using System.Linq;

namespace iOrder.Application.Orders.Queries.GetOrders
{
    public class OrderViewModel : IHaveCustomMapping
    {
        public Guid OrderId { get; set; }

        public string Number { get; set; }

        public DateTime Created { get; set; }

        public double Total { get; set; }

        public string User { get; set; }

        public Guid PointId { get; set; }

        public string PointAddress { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Order, OrderViewModel>()
                .ForMember(v => v.OrderId, x => x.MapFrom(m => m.Id))
                .ForMember(v => v.Total, x => x.MapFrom(m => m.OrderItems.Sum(x => x.Price * x.Quantity)))
                .ForMember(v => v.User, x => x.MapFrom(m => m.User != null ? m.User.Name : string.Empty))
                .ForMember(v => v.PointAddress, x => x.MapFrom(m => m.Point != null ? m.Point.Address : string.Empty))
                ;
        }
    }
}