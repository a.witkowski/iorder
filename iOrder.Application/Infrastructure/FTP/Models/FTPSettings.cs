﻿using System;

namespace iOrder.Application.Infrastructure.FTP.Models
{
    public class FTPSettings
    {
        public Uri Host { get; set; }

        public string Folder { get; set; }

        public string User { get; set; }

        public string Password { get; set; }
    }
}
