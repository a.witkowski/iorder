﻿namespace iOrder.Application.Infrastructure.Tokens
{
    public class JWTToken
    {
        public string Secret { get; set; }

        public string Issuer { get; set; }

        public string Audience { get; set; }

        public int Expires { get; set; }

        public int Refresh { get; set; }
    }
}
