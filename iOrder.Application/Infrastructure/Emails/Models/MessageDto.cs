﻿using System.Collections.Generic;

namespace iOrder.Application.Emails.Models
{
    public class MessageDto
    {
        public string From { get; set; }
        public IList<string> To { get; set; }
        public IList<string> Cc { get; set; }
        public IList<string> Bcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
