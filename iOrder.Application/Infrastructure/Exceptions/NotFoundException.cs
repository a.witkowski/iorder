﻿using System;

namespace iOrder.Application.Infrastructure.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string name, object key)
            : base($"{name} {key} не найден")
        {

        }
    }
}
