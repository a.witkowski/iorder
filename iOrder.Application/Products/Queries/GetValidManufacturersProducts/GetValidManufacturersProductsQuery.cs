﻿using MediatR;
using System.Collections.Generic;

namespace iOrder.Application.Products.Queries.GetValidManufacturersProducts
{
    public class GetValidManufacturersProductsQuery : IRequest<IEnumerable<ValidManufacturersProductsViewModel>>
    {
    }
}
