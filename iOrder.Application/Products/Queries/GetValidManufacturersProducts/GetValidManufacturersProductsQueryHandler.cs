﻿using AutoMapper;
using iOrder.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Products.Queries.GetValidManufacturersProducts
{
    public class GetValidManufacturersProductsQueryHandler : IRequestHandler<GetValidManufacturersProductsQuery, IEnumerable<ValidManufacturersProductsViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetValidManufacturersProductsQueryHandler> _logger;

        public GetValidManufacturersProductsQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetValidManufacturersProductsQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<ValidManufacturersProductsViewModel>> Handle(GetValidManufacturersProductsQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetValidManufacturersProductsQueryHandler Start");

            try
            {
                var products = await _context.Products
                        .Include(x => x.Factory)
                            .ThenInclude(x => x.Manufacturer)
                        .Where(x => x.ProductPrices.Any())
                        .OrderBy(x => x.Factory.Manufacturer.Name)
                        .ThenBy(x => x.Name)
                        .ToListAsync(cancellationToken);

                var model = _mapper.Map<IEnumerable<ValidManufacturersProductsViewModel>>(products);

                _logger.LogInformation("GetValidManufacturersProductsQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
