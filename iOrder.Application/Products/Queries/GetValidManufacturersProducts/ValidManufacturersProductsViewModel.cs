﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;

namespace iOrder.Application.Products.Queries.GetValidManufacturersProducts
{
    public class ValidManufacturersProductsViewModel : IHaveCustomMapping
    {
        public Guid ManufacturerId { get; set; }

        public Guid ProductId { get; set; }

        public string Manufacturer { get; set; }

        public string ProductName { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Product, ValidManufacturersProductsViewModel>()
                .ForMember(v => v.ManufacturerId, x => x.MapFrom(m => m.Factory.Manufacturer.Id))
                .ForMember(v => v.ProductId, x => x.MapFrom(m => m.Id))
                .ForMember(v => v.Manufacturer, x => x.MapFrom(m => m.Factory.Manufacturer.Name))
                .ForMember(v => v.ProductName, x => x.MapFrom(m => m.Name))
                ;
        }
    }
}
