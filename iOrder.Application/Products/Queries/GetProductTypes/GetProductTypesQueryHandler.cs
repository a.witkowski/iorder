﻿using AutoMapper;
using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Products.Queries.GetProductTypes
{
    public class GetProductTypesQueryHandler : IRequestHandler<GetProductTypesQuery, IEnumerable<ProductTypeViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetProductTypesQueryHandler> _logger;

        public GetProductTypesQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetProductTypesQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<ProductTypeViewModel>> Handle(GetProductTypesQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetProductTypesQueryHandler Start");

            try
            {
                User user = await _context.Users
                    .Include(x => x.UserSetting)
                    .Include(x => x.Organization)
                        .ThenInclude(x => x.OrganizationSetting)
                    .Include(x => x.Organization.Contracts)
                    .Include("Organization.Contracts.ContractType")
                    .SingleOrDefaultAsync(x => x.Id == request.UserId);
                if (user == null)
                {
                    _logger.LogError($"User {request.UserId} not found");
                    throw new NotFoundException(nameof(User), request.UserId);
                }

                var queryTypes = _context.ProductTypes.AsQueryable();

                if (user.Organization.Contracts.Count == 1)
                {
                    var contract = user.Organization.Contracts.FirstOrDefault();
                    switch (contract.ContractType.Type.ToLower())
                    {
                        case "аптечный":
                            queryTypes = queryTypes.Where(x => x.Type.ToLower() != "косметика");
                            break;
                        case "косметика":
                            queryTypes = queryTypes.Where(x => x.Type.ToLower() == "косметика");
                            break;
                    }
                }

                var types = await queryTypes.OrderBy(x => x.Id).ToListAsync(cancellationToken);

                IEnumerable<ProductType> productTypes = new List<ProductType>();

                if (user.UserSetting.UsePharmacy)
                {
                    productTypes = productTypes.Union(types.Where(x => string.Equals(x.Type, "аптечный", StringComparison.OrdinalIgnoreCase)));
                }

                if (user.UserSetting.UseDietarySupplement)
                {
                    productTypes = productTypes.Union(types.Where(x => string.Equals(x.Type, "бад", StringComparison.OrdinalIgnoreCase)));
                }

                if (user.UserSetting.UseCosmetic)
                {
                    productTypes = productTypes.Union(types.Where(x => string.Equals(x.Type, "косметика", StringComparison.OrdinalIgnoreCase)));
                }

                if (user.UserSetting.UseOptics)
                {
                    productTypes = productTypes.Union(types.Where(x => string.Equals(x.Type, "оптика", StringComparison.OrdinalIgnoreCase)));
                }

                if (user.UserSetting.UseMedicalEquipment)
                {
                    productTypes = productTypes.Union(types.Where(x => string.Equals(x.Type, "мт", StringComparison.OrdinalIgnoreCase)));
                }

                if (user.UserSetting.UseMedicalProducts)
                {
                    productTypes = productTypes.Union(types.Where(x => string.Equals(x.Type, "имн", StringComparison.OrdinalIgnoreCase)));
                }

                var model = _mapper.Map<IEnumerable<ProductTypeViewModel>>(productTypes);

                _logger.LogInformation("GetProductTypesQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
