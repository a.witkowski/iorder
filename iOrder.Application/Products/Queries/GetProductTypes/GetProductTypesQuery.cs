﻿using MediatR;
using System;
using System.Collections.Generic;

namespace iOrder.Application.Products.Queries.GetProductTypes
{
    public class GetProductTypesQuery : IRequest<IEnumerable<ProductTypeViewModel>>
    {
        public Guid UserId { get; set; }
    }
}
