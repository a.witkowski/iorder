﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;

namespace iOrder.Application.Products.Queries.GetProductTypes
{
    public class ProductTypeViewModel : IHaveCustomMapping
    {
        public int ProductTypeId { get; set; }

        public string Type { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<ProductType, ProductTypeViewModel>()
                .ForMember(v => v.ProductTypeId, x => x.MapFrom(m => m.Id))
                ;
        }
    }
}
