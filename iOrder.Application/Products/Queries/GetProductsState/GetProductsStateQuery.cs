﻿using MediatR;
using System;
using System.Collections.Generic;

namespace iOrder.Application.Products.Queries.GetProductsState
{
    public class GetProductsStateQuery : IRequest<IEnumerable<ProductStateViewModel>>
    {
        public Guid UserId { get; set; }
    }
}
