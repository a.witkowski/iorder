﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;

namespace iOrder.Application.Products.Queries.GetProductsState
{
    public class ProductStateViewModel : IHaveCustomMapping
    {
        public Guid ProductId { get; set; }

        public Guid PointId { get; set; }

        public Guid OrderId { get; set; }

        public int Quantity { get; set; }

        public string State { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<OrderItem, ProductStateViewModel>()
                .ForMember(v => v.PointId, x => x.MapFrom(m => m.Order.PointId))
                .ForMember(v => v.State, x => x.MapFrom(m => m.OrderItemState != null ? m.OrderItemState.State : string.Empty))
                ;
        }
    }
}
