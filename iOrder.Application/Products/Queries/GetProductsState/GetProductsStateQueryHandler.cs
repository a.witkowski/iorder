﻿using AutoMapper;
using iOrder.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Products.Queries.GetProductsState
{
    public class GetProductsStateQueryHandler : IRequestHandler<GetProductsStateQuery, IEnumerable<ProductStateViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetProductsStateQueryHandler> _logger;

        public GetProductsStateQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetProductsStateQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<ProductStateViewModel>> Handle(GetProductsStateQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetProductsStatusQueryHandler Start");

            try
            {
                var statuses = await _context.OrderItems
                    .Include(x => x.OrderItemState)
                    .Include(x => x.Order)
                    .Where(x => x.Order.UserId == request.UserId && x.Order.Created >= DateTime.Now.AddMonths(-1))
                    .ToListAsync(cancellationToken);

                var model = _mapper.Map<IEnumerable<ProductStateViewModel>>(statuses);

                _logger.LogInformation("GetProductsStatusQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);

                throw;
            }
        }
    }
}
