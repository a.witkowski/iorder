﻿using AutoMapper;
using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Products.Queries.GetProductPrices
{
    public class GetProductPricesQueryHandler : IRequestHandler<GetProductPricesQuery, IEnumerable<ProductPriceViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetProductPricesQueryHandler> _logger;

        public GetProductPricesQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetProductPricesQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<ProductPriceViewModel>> Handle(GetProductPricesQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetProductPricesQueryHandler Start");

            User user = await _context.Users
                .Include(x => x.UserSetting)
                .Include(x => x.Organization)
                    .ThenInclude(x => x.OrganizationType)
                .Include(x => x.Organization.OrganizationSetting)
                .Include(x => x.Organization.Contracts)
                .Include("Organization.Contracts.ContractType")
                .Include(x => x.Organization.OrganizationPriceSettings)
                .SingleOrDefaultAsync(x => x.Id == request.UserId);
            if (user == null)
            {
                _logger.LogError($"User {request.UserId} not found");
                throw new NotFoundException(nameof(User), request.UserId);
            }

            try
            {
                var queryPrices = _context.ProductPrices.AsQueryable();

                switch (user.Organization.OrganizationType.Type)
                {
                    case "Опт":
                        queryPrices = queryPrices.Where(x => (x.Product.Factory.ManufacturerId == null
                                                             || x.Product.Factory.Manufacturer.HideWholesale == false)
                                                             && x.Quantity > 0);

                        if (user.Organization.OrganizationSetting.UsePriceSettings)
                            queryPrices = queryPrices.Where(x => !x.Product.Factory.OrganizationPriceSettings
                                                                 .Any(o => o.OrganizationId == user.OrganizationId && o.HideWholesale && o.Deleted == false));
                        break;
                    case "Партнерская розница":
                        queryPrices = queryPrices.Where(x => (x.Product.Factory.ManufacturerId == null || x.Product.Factory.Manufacturer.HideRetail == false)
                            && x.QuantityPartner > 0);

                        if (user.Organization.OrganizationSetting.UsePriceSettings)
                            queryPrices = queryPrices.Where(x => !x.Product.Factory.OrganizationPriceSettings.Any(o => o.OrganizationId == user.OrganizationId && o.HideRetail && o.Deleted == false));
                        break;
                    case "Бартерный":
                        queryPrices = queryPrices.Where(x => (x.Product.Factory.ManufacturerId == null || x.Product.Factory.Manufacturer.HideBarter == false)
                            && x.QuantityBarter > 0);

                        if (user.Organization.OrganizationSetting.UsePriceSettings)
                            queryPrices = queryPrices.Where(x => !x.Product.Factory.OrganizationPriceSettings.Any(o => o.OrganizationId == user.OrganizationId && o.HideBarter && o.Deleted == false));
                        break;
                }

                if (user.Organization.Contracts.Count == 1)
                {
                    var contract = user.Organization.Contracts.FirstOrDefault();
                    switch (contract.ContractType.Type.ToLower())
                    {
                        case "аптечный":
                            queryPrices = queryPrices.Where(x => x.Product.ProductType.Type.ToLower() == "аптечный"
                                || x.Product.ProductType.Type.ToLower() == "бад"
                                || x.Product.ProductType.Type.ToLower() == "оптика"
                                || x.Product.ProductType.Type.ToLower() == "имн"
                                || x.Product.ProductType.Type.ToLower() == "мт");
                            break;
                        case "косметика":
                            queryPrices = queryPrices.Where(x => x.Product.ProductType.Type.ToLower() == "косметика");
                            break;
                    }
                }

                IList<ProductPrice> pricesDb = await queryPrices
                    .Include(x => x.Product)
                        .ThenInclude(x => x.Factory)
                            .ThenInclude(x => x.Manufacturer)
                    .Include(x => x.Product.ProductType)
                    .OrderBy(x => x.Product.Name)
                    .ToListAsync(cancellationToken);

                IEnumerable<ProductPrice> prices = new List<ProductPrice>();

                if (user.Organization.OrganizationSetting.UsePharmacy && user.UserSetting.UsePharmacy)
                {
                    prices = prices.Union(pricesDb.Where(x => string.Equals(x.Product.ProductType.Type, "аптечный", StringComparison.OrdinalIgnoreCase)));
                }

                if (user.Organization.OrganizationSetting.UseDietarySupplement && user.UserSetting.UseDietarySupplement)
                {
                    prices = prices.Union(pricesDb.Where(x => string.Equals(x.Product.ProductType.Type, "бад", StringComparison.OrdinalIgnoreCase)));
                }

                if (user.Organization.OrganizationSetting.UseCosmetic && user.UserSetting.UseCosmetic)
                {
                    prices = prices.Union(pricesDb.Where(x => string.Equals(x.Product.ProductType.Type, "косметика", StringComparison.OrdinalIgnoreCase)));
                }

                if (user.Organization.OrganizationSetting.UseOptics && user.UserSetting.UseOptics)
                {
                    prices = prices.Union(pricesDb.Where(x => string.Equals(x.Product.ProductType.Type, "оптика", StringComparison.OrdinalIgnoreCase)));
                }

                if (user.Organization.OrganizationSetting.UseMedicalEquipment && user.UserSetting.UseMedicalEquipment)
                {
                    prices = prices.Union(pricesDb.Where(x => string.Equals(x.Product.ProductType.Type, "мт", StringComparison.OrdinalIgnoreCase)));
                }

                if (user.Organization.OrganizationSetting.UseMedicalProducts && user.UserSetting.UseMedicalProducts)
                {
                    prices = prices.Union(pricesDb.Where(x => string.Equals(x.Product.ProductType.Type, "имн", StringComparison.OrdinalIgnoreCase)));
                }

                switch (user.Organization.OrganizationType.Type)
                {
                    case "Опт":
                        break;
                    case "Партнерская розница":
                        prices.All(x =>
                        {
                            x.Lot = x.LotPartner;
                            x.Expiration = x.ExpirationPartner;
                            x.Price = x.PricePartner;
                            x.Quantity = x.QuantityPartner;
                            return true;
                        });
                        break;
                    case "Бартерный":
                        prices.All(x =>
                        {
                            x.Lot = x.LotBarter;
                            x.Expiration = x.ExpirationBarter;
                            x.Price = x.PriceBarter;
                            x.Quantity = 0;
                            return true;
                        });
                        break;
                }

                List<ProductPrice> reducedPrices = new List<ProductPrice>();

                var temp = prices.OrderBy(x => x.Product.Name).GroupBy(x => x.ProductId).AsParallel();
                foreach (var item in temp)
                {
                    var i = item.OrderBy(x => x.Expiration).ThenBy(x => x.Quantity);
                    if (i.First().Quantity <= 10)
                        reducedPrices.AddRange(i.Take(2));
                    else
                        reducedPrices.Add(i.First());
                }

                var model = _mapper.Map<IEnumerable<ProductPriceViewModel>>(reducedPrices);

                _logger.LogInformation("GetProductPricesQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
