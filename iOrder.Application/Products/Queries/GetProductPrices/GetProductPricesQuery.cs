﻿using MediatR;
using System;
using System.Collections.Generic;

namespace iOrder.Application.Products.Queries.GetProductPrices
{
    public class GetProductPricesQuery : IRequest<IEnumerable<ProductPriceViewModel>>
    {
        public Guid UserId { get; set; }
    }
}
