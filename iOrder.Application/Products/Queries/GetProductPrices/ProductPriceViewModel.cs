﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;

namespace iOrder.Application.Products.Queries.GetProductPrices
{
    public class ProductPriceViewModel : IHaveCustomMapping
    {
        public Guid ProductId { get; set; }

        public string Name { get; set; }

        public string Manufacturer { get; set; }

        public int? PriceGroup { get; set; }

        public string Country { get; set; }

        public double Price { get; set; }

        public DateTime? Expiration { get; set; }

        public int Quantity { get; set; }

        public string Lot { get; set; }

        public int ProductTypeId { get; set; }

        public bool IsColdZone { get; set; }

        public bool IsExpectedArrival { get; set; }

        public bool IsPrescription { get; set; }

        public bool IsEnrollPrice { get; set; }

        public bool IsPromo { get; set; }

        public string Pack { get; set; }

        public DateTime Updated { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<ProductPrice, ProductPriceViewModel>()
                .ForMember(v => v.Name, x => x.MapFrom(m => m.Product != null ? m.Product.Name : string.Empty))
                .ForMember(v => v.Manufacturer, x => x.MapFrom(m => m.Product != null && m.Product.Factory != null && m.Product.Factory.Manufacturer != null
                    ? $"{ m.Product.Factory.Manufacturer.Name } ({ m.Product.Factory.Name })"
                    : string.Empty))
                .ForMember(v => v.PriceGroup, x => x.MapFrom(m => m.Product != null && m.Product.Factory != null && m.Product.Factory.Manufacturer != null
                    ? m.Product.Factory.Manufacturer.PriceGroup : null))
                .ForMember(v => v.Country, x => x.MapFrom(m => m.Product != null && m.Product.Factory != null ? m.Product.Factory.Country : string.Empty))
                .ForMember(v => v.ProductTypeId, x => x.MapFrom(m => m.Product != null ? m.Product.ProductTypeId : 0))
                .ForMember(v => v.IsColdZone, x => x.MapFrom(m => m.Product != null ? m.Product.IsColdZone : false))
                .ForMember(v => v.IsPrescription, x => x.MapFrom(m => m.Product != null ? m.Product.IsPrescription : false))
                .ForMember(v => v.IsEnrollPrice, x => x.MapFrom(m => m.Product != null ? m.Product.IsEnrollPrice : false))
                .ForMember(v => v.Pack, x => x.MapFrom(m => m.Product != null ? m.Product.Pack : string.Empty))
                ;
        }
    }
}
