﻿using MediatR;
using System;

namespace iOrder.Application.Promotions.Commands.UpdatePromotion
{
    public class UpdatePromotionCommand : IRequest
    {
        public Guid PromotionId { get; set; }

        public int? OrganizationTypeId { get; set; }

        public Guid? OrganizationId { get; set; }

        public string Name { get; set; }

        public DateTime? Start { get; set; }

        public DateTime? End { get; set; }
    }
}
