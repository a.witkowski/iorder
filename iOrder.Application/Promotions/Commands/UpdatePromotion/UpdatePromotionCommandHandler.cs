﻿using AutoMapper;
using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Promotions.Commands.UpdatePromotion
{
    public class UpdatePromotionCommandHandler : IRequestHandler<UpdatePromotionCommand, Unit>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdatePromotionCommandHandler> _logger;

        public UpdatePromotionCommandHandler(IOrderDbContext context,
            IMapper mapper,
            ILogger<UpdatePromotionCommandHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Unit> Handle(UpdatePromotionCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("UpdatePromotionCommandHandler Start");

            var promotion = await _context.Promotions
                .Include(x => x.OrganizationPromotions)
                .SingleOrDefaultAsync(x => x.Id == request.PromotionId);
            if (promotion == null)
            {
                _logger.LogInformation($"Promotion {request.PromotionId} not found");
                throw new NotFoundException("Акция", request.PromotionId);
            }

            try
            {
                var currentData = DateTime.Now;

                promotion.Name = request.Name;
                promotion.Start = request.Start ?? DateTime.Now.Date;
                promotion.End = request.End ?? DateTime.Now.AddYears(1).Date;
                promotion.Updated = currentData;
                promotion.OrganizationPromotions.Clear();

                // for one client
                if (request.OrganizationId.HasValue)
                {
                    promotion.OrganizationPromotions.Add(new OrganizationPromotion() { OrganizationId = request.OrganizationId.Value });
                }
                // for clients types
                else if (request.OrganizationTypeId.HasValue)
                {
                    IList<Guid> organizationsIds = await _context.Organizations
                        .Where(x => x.OrganizationTypeId == request.OrganizationTypeId.Value)
                        .Select(x => x.Id)
                        .ToListAsync();
                    if (organizationsIds != null && organizationsIds.Count > 0)
                    {
                        foreach (var id in organizationsIds)
                        {
                            promotion.OrganizationPromotions.Add(new OrganizationPromotion() { OrganizationId = id });
                        }
                    }
                }
                // for all
                else
                {
                    IList<Guid> organizationsIds = await _context.Organizations.Select(x => x.Id).ToListAsync();
                    if (organizationsIds != null && organizationsIds.Count > 0)
                    {
                        foreach (var id in organizationsIds)
                        {
                            promotion.OrganizationPromotions.Add(new OrganizationPromotion() { OrganizationId = id });
                        }
                    }
                }

                await _context.SaveChangesAsync(cancellationToken);

                _logger.LogInformation("UpdatePromotionCommandHandler End");

                return Unit.Value;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
