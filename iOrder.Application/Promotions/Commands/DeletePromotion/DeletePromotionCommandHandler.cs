﻿using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Promotions.Commands.DeletePromotion
{
    public class DeletePromotionCommandHandler : IRequestHandler<DeletePromotionCommand, Unit>
    {
        private readonly IOrderDbContext _context;
        private readonly ILogger<DeletePromotionCommandHandler> _logger;

        public DeletePromotionCommandHandler(
            IOrderDbContext context,
            ILogger<DeletePromotionCommandHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Unit> Handle(DeletePromotionCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("DeletePromotionCommandHandler Start");

            try
            {
                var entity = await _context.Promotions.FindAsync(request.PromotionId);
                if (entity == null)
                {
                    _logger.LogInformation($"Promotion {request.PromotionId} not found");
                    throw new NotFoundException(nameof(Promotion), request.PromotionId);
                }

                _context.Promotions.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                _logger.LogInformation("DeletePromotionCommandHandler End");

                return Unit.Value;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
