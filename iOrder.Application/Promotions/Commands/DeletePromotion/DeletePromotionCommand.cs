﻿using MediatR;
using System;

namespace iOrder.Application.Promotions.Commands.DeletePromotion
{
    public class DeletePromotionCommand : IRequest
    {
        public Guid PromotionId { get; set; }
    }
}
