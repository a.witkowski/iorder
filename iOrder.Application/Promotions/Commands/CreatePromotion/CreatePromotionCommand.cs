﻿using MediatR;
using System;

namespace iOrder.Application.Promotions.Commands.CreatePromotion
{
    public class CreatePromotionCommand : IRequest
    {
        public int? OrganizationTypeId { get; set; }

        public Guid? OrganizationId { get; set; }

        public DateTime? Start { get; set; }

        public DateTime? End { get; set; }

        public string FileName { get; set; }

        public string Link { get; set; }
    }
}
