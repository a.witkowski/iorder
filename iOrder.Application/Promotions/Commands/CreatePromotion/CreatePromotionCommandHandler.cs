﻿using AutoMapper;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Promotions.Commands.CreatePromotion
{
    public class CreatePromotionCommandHandler : IRequestHandler<CreatePromotionCommand, Unit>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<CreatePromotionCommandHandler> _logger;

        public CreatePromotionCommandHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<CreatePromotionCommandHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Unit> Handle(CreatePromotionCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("CreatePromotionCommandHandler Start");

            var currentData = DateTime.Now;

            try
            {
                var promotion = new Promotion()
                {
                    Name = request.FileName,
                    Link = request.Link,
                    Start = request.Start ?? DateTime.Now.Date,
                    End = request.End ?? DateTime.Now.AddYears(1).Date,
                    Created = currentData,
                    Updated = currentData,
                };

                // for one client
                if (request.OrganizationId.HasValue)
                {
                    promotion.OrganizationPromotions.Add(new OrganizationPromotion() { OrganizationId = request.OrganizationId.Value });
                }
                // for clients types
                else if (request.OrganizationTypeId.HasValue)
                {
                    IList<Guid> organizationsIds = await _context.Organizations.Where(x => x.OrganizationTypeId == request.OrganizationTypeId.Value).Select(x => x.Id).ToListAsync();
                    if (organizationsIds != null && organizationsIds.Count > 0)
                    {
                        foreach (var id in organizationsIds)
                        {
                            promotion.OrganizationPromotions.Add(new OrganizationPromotion() { OrganizationId = id });
                        }
                    }
                }
                // for all
                else
                {
                    IList<Guid> organizationsIds = await _context.Organizations.Select(x => x.Id).ToListAsync();
                    if (organizationsIds != null && organizationsIds.Count > 0)
                    {
                        foreach (var id in organizationsIds)
                        {
                            promotion.OrganizationPromotions.Add(new OrganizationPromotion() { OrganizationId = id });
                        }
                    }
                }

                _context.Promotions.Add(promotion);

                await _context.SaveChangesAsync(cancellationToken);

                _logger.LogInformation("CreatePromotionCommandHandler End");

                return Unit.Value;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
