﻿using AutoMapper;
using iOrder.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Promotions.Queries.GetValidPromotions
{
    public class GetValidPromotionsQueryHandler : IRequestHandler<GetValidPromotionsQuery, IEnumerable<ValidPromotionViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetValidPromotionsQueryHandler> _logger;

        public GetValidPromotionsQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetValidPromotionsQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<ValidPromotionViewModel>> Handle(GetValidPromotionsQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetValidPromotionsQueryHandler Start");

            try
            {
                var promotions = await _context.OrganizationPromotions
                        .Where(x => x.Organization.Users.Any(u => u.Id == request.UserId) && x.Promotion.Start.Date <= DateTime.Today && x.Promotion.End.Date >= DateTime.Today)
                        .Select(x => x.Promotion)
                        .OrderByDescending(x => x.Created)
                        .ToListAsync(cancellationToken);

                var model = _mapper.Map<IEnumerable<ValidPromotionViewModel>>(promotions);

                _logger.LogInformation("GetValidPromotionsQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
