﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;

namespace iOrder.Application.Promotions.Queries.GetValidPromotions
{
    public class ValidPromotionViewModel : IHaveCustomMapping
    {
        public Guid PromotionId { get; set; }

        public string Name { get; set; }

        public string Link { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Promotion, ValidPromotionViewModel>()
                .ForMember(v => v.PromotionId, x => x.MapFrom(m => m.Id))
                ;
        }
    }
}
