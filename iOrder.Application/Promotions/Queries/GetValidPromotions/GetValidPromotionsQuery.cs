﻿using MediatR;
using System;
using System.Collections.Generic;

namespace iOrder.Application.Promotions.Queries.GetValidPromotions
{
    public class GetValidPromotionsQuery : IRequest<IEnumerable<ValidPromotionViewModel>>
    {
        public Guid UserId { get; set; }
    }
}
