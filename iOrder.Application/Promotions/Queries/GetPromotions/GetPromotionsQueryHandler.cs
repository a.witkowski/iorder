﻿using AutoMapper;
using iOrder.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Promotions.Queries.GetPromotions
{
    public class GetPromotionsQueryHandler : IRequestHandler<GetPromotionsQuery, IEnumerable<PromotionViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetPromotionsQueryHandler> _logger;

        public GetPromotionsQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetPromotionsQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<PromotionViewModel>> Handle(GetPromotionsQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetPromotionsQueryHandler Start");

            try
            {
                var promotions = await _context.Promotions
                    .Include(x => x.OrganizationPromotions)
                    .Include("OrganizationPromotions.Organization")
                    .Include("OrganizationPromotions.Organization.OrganizationType")
                    .OrderByDescending(x => x.Created)
                    .ToListAsync(cancellationToken);

                IEnumerable<PromotionViewModel> model = _mapper.Map<IEnumerable<PromotionViewModel>>(promotions);

                _logger.LogInformation("GetPromotionsQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
