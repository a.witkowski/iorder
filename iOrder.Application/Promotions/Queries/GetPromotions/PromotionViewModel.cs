﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;
using System.Linq;

namespace iOrder.Application.Promotions.Queries.GetPromotions
{
    public class PromotionViewModel : IHaveCustomMapping
    {
        public Guid PromotionId { get; set; }

        public string Name { get; set; }

        public string Link { get; set; }

        public bool Deleted { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public int? OrganizationTypeId { get; set; }

        public Guid? OrganizationId { get; set; }

        public string OrganizationType { get; set; }

        public string OrganizationName { get; set; }

        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Promotion, PromotionViewModel>()
                .ForMember(v => v.PromotionId, x => x.MapFrom(m => m.Id))
                .ForMember(v => v.OrganizationTypeId, x => x.MapFrom(
                    m => m.OrganizationPromotions.Select(p => p.Organization.OrganizationTypeId).Distinct().Count() == 1
                    ? m.OrganizationPromotions.Select(p => p.Organization.OrganizationTypeId).Distinct().Cast<int?>().SingleOrDefault()
                    : null))
                .ForMember(v => v.OrganizationId, x => x.MapFrom(
                    m => m.OrganizationPromotions.Select(p => p.OrganizationId).Count() == 1
                    ? m.OrganizationPromotions.Select(p => p.OrganizationId).Distinct().Cast<Guid?>().SingleOrDefault()
                    : null))
                .ForMember(v => v.OrganizationType, x => x.MapFrom(
                    m => m.OrganizationPromotions.Select(p => p.Organization).Select(o => o.OrganizationType.Type).Distinct().Count() >= 3
                    ? "Все"
                    : string.Join(", ", m.OrganizationPromotions.Select(p => p.Organization).Select(o => o.OrganizationType.Type).Distinct()).Trim(',')))
                .ForMember(v => v.OrganizationName, x => x.MapFrom(
                    m => m.OrganizationPromotions.Select(p => p.Organization.Name).Distinct().Count() > 1
                    ? string.Empty
                    : string.Join(", ", m.OrganizationPromotions.Select(p => p.Organization.Name).Distinct()).Trim(',')))
                ;
        }
    }
}
