﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace iOrder.Application.Promotions.Queries.GetPromotions
{
    public class GetPromotionsQuery : IRequest<IEnumerable<PromotionViewModel>>
    {
    }
}
