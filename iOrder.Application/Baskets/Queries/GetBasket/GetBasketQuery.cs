﻿using MediatR;
using System;
using System.Collections.Generic;

namespace iOrder.Application.Baskets.Queries.GetBasket
{
    public class GetBasketQuery : IRequest<IEnumerable<BasketViewModel>>
    {
        public Guid UserId { get; set; }
    }
}
