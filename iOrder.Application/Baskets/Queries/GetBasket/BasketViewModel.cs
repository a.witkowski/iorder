﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;

namespace iOrder.Application.Baskets.Queries.GetBasket
{
    public class BasketViewModel : IHaveCustomMapping
    {
        public Guid UserId { get; set; }

        public Guid PointId { get; set; }

        public Guid ProductId { get; set; }

        public string ProductName { get; set; }

        public string Manufacturer { get; set; }

        public string Factory { get; set; }

        public string ProductCountry { get; set; }

        public string PointAddress { get; set; }

        public string Lot { get; set; }

        public DateTime? Expiration { get; set; }

        public double Price { get; set; }

        public int Quantity { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Basket, BasketViewModel>()
                .ForMember(v => v.ProductName, x => x.MapFrom(m => m.Product != null ? m.Product.Name : string.Empty))
                .ForMember(v => v.Manufacturer, x => x.MapFrom(m => m.Product != null && m.Product.Factory != null && m.Product.Factory.Manufacturer != null
                    ? m.Product.Factory.Manufacturer.Name
                    : string.Empty))
                .ForMember(v => v.Factory, x => x.MapFrom(m => m.Product != null && m.Product.Factory != null ? m.Product.Factory.Name : string.Empty))
                .ForMember(v => v.ProductCountry, x => x.MapFrom(m => m.Product != null && m.Product.Factory != null ? m.Product.Factory.Country : string.Empty))
                .ForMember(v => v.PointAddress, x => x.MapFrom(m => m.Point != null ? m.Point.Address : string.Empty))
                ;
        }
    }
}
