﻿using AutoMapper;
using iOrder.Application.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Baskets.Queries.GetBasket
{
    public class GetBasketQueryHandler : IRequestHandler<GetBasketQuery, IEnumerable<BasketViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetBasketQueryHandler> _logger;

        public GetBasketQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetBasketQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<BasketViewModel>> Handle(GetBasketQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetBasketQueryHandler Start");

            if (string.IsNullOrWhiteSpace(request.UserId.ToString()) || request.UserId == Guid.Empty)
            {
                _logger.LogInformation($"GetBasketQueryHandler: UserId={request.UserId}");
                throw new Exception("GetBasketQueryHandler: UserId is empty");
            }

            var basket = await _context.Baskets
                .Include(x => x.Product)
                        .ThenInclude(x => x.Factory)
                            .ThenInclude(x => x.Manufacturer)
                .Include(x => x.Point)
                .Where(x => x.UserId == request.UserId)
                .OrderBy(x => x.Point.Address)
                    .ThenBy(x => x.Product.Name)
                .ToListAsync(cancellationToken);

            var model = _mapper.Map<IEnumerable<BasketViewModel>>(basket);

            _logger.LogInformation("GetBasketQueryHandler End");

            return model;
        }
    }
}
