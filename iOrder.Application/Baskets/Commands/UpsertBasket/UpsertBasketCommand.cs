﻿using MediatR;
using System;

namespace iOrder.Application.Baskets.Commands.UpsertBasket
{
    public class UpsertBasketCommand : IRequest<Guid>
    {
        public Guid UserId { get; set; }

        public Guid PointId { get; set; }

        public Guid ProductId { get; set; }

        public string Lot { get; set; }

        public DateTime? Expiration { get; set; }

        public double Price { get; set; }

        public int Quantity { get; set; }
    }
}
