﻿using AutoMapper;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Baskets.Commands.UpsertBasket
{
    public class UpsertBasketCommandHandler : IRequestHandler<UpsertBasketCommand, Guid>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<UpsertBasketCommandHandler> _logger;

        public UpsertBasketCommandHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<UpsertBasketCommandHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Guid> Handle(UpsertBasketCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("CreateBasketCommandHandler Start");

            if (request == null)
            {
                _logger.LogError("CreateBasketCommandHandler: BasketItem is empty");
                throw new Exception("CreateBasketCommandHandler: BasketItem is empty");
            }

            _logger.LogInformation($"CreateBasketCommandHandler: UserId={request.UserId}" +
                $", PointId={request.PointId}" +
                $", ProductId={request.ProductId}" +
                $", Lot={request.Lot}" +
                $", Expiration={request.Expiration}" +
                $", Price={request.Price}" +
                $", Quantity={request.Quantity}");

            try
            {
                var currentData = DateTime.Now;

                Basket basket = await _context.Baskets.SingleOrDefaultAsync(x => x.UserId == request.UserId && x.PointId == request.PointId && x.ProductId == request.ProductId);
                if (basket == null)
                {
                    basket = new Basket
                    {
                        UserId = request.UserId,
                        PointId = request.PointId,
                        ProductId = request.ProductId,
                        Lot = request.Lot,
                        Expiration = request.Expiration,
                        Price = request.Price,
                        Created = currentData
                    };

                    _context.Baskets.Add(basket);
                }

                if (request.Quantity > 0)
                    basket.Quantity = request.Quantity;
                else
                    _context.Baskets.Remove(basket);

                await _context.SaveChangesAsync(cancellationToken);

                _logger.LogInformation("CreateBasketCommandHandler End");
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, ex.Message, ex.StackTrace);
                throw new Exception(ex.Message, ex.InnerException);
            }

            return Guid.Empty;
        }
    }
}
