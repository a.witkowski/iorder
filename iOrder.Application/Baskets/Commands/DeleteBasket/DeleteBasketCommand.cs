﻿using MediatR;
using System;

namespace iOrder.Application.Baskets.Commands.DeleteBasket
{
    public class DeleteBasketCommand : IRequest
    {
        public Guid UserId { get; set; }
    }
}
