﻿using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Baskets.Commands.DeleteBasket
{
    public class DeleteBasketCommandHandler : IRequestHandler<DeleteBasketCommand>
    {
        private readonly IOrderDbContext _context;
        private readonly ILogger<DeleteBasketCommandHandler> _logger;

        public DeleteBasketCommandHandler(
            IOrderDbContext context,
            ILogger<DeleteBasketCommandHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Unit> Handle(DeleteBasketCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("DeleteBasketCommandHandler Start");

            if (string.IsNullOrWhiteSpace(request.UserId.ToString()) || request.UserId == Guid.Empty)
            {
                _logger.LogInformation($"DeleteBasketCommandHandler: UserId={request.UserId}");
                throw new Exception("DeleteBasketCommandHandler: UserId is empty");
            }

            User user = await _context.Users.SingleOrDefaultAsync(x => x.Id == request.UserId && !x.Deleted);
            if (user == null)
            {
                _logger.LogInformation($"User {request.UserId} not found");
                throw new NotFoundException(nameof(User), request.UserId);
            }

            try
            {
                var basket = await _context.Baskets
                    .Where(x => x.UserId == request.UserId)
                    .ToListAsync(cancellationToken);

                _context.Baskets.RemoveRange(basket);

                await _context.SaveChangesAsync(cancellationToken);

                _logger.LogInformation("DeleteBasketCommandHandler End");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
            }

            return Unit.Value;
        }
    }
}
