﻿using MediatR;
using System;

namespace iOrder.Application.Users.Queries.GetUser
{
    public class GetUserQuery : IRequest<UserViewModel>
    {
        public Guid UserId { get; set; }
    }
}
