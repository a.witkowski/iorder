﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iOrder.Application.Users.Queries.GetUser
{
    public class UserViewModel : IHaveCustomMapping
    {
        public Guid UserId { get; set; }

        public string Login { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public Guid OrganizationId { get; set; }

        public string OrganizationName { get; set; }

        public IList<string> UserRoles { get; set; }

        public object Settings { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<User, UserViewModel>()
                .ForMember(v => v.UserId, x => x.MapFrom(m => m.Id))
                .ForMember(v => v.OrganizationName, x => x.MapFrom(m => m.Organization != null ? m.Organization.Name : string.Empty))
                .ForMember(v => v.UserRoles, x => x.MapFrom(m => m.UserRoles != null ? m.UserRoles.Select(r => r.Role.Name) : null))
                .ForMember(v => v.Settings, x => x.MapFrom(m => m.UserSetting))
                ;
        }
    }
}
