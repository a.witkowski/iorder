﻿using AutoMapper;
using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Users.Queries.GetUser
{
    public class GetUserQueryHandler : IRequestHandler<GetUserQuery, UserViewModel>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetUserQueryHandler> _logger;

        public GetUserQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetUserQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<UserViewModel> Handle(GetUserQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetUserQueryHandler Start");

            try
            {
                User user = await _context.Users
                    .Include(x => x.Organization)
                    .Include(x => x.UserRoles)
                    .Include(x => x.UserSetting)
                    .Include("UserRoles.Role")
                    .SingleOrDefaultAsync(x => x.Id == request.UserId);

                if (user == null)
                {
                    _logger.LogInformation($"User {request.UserId} not found");
                    throw new NotFoundException(nameof(User), request.UserId);
                }

                var model = _mapper.Map<UserViewModel>(user);

                _logger.LogInformation("GetUserQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
