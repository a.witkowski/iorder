﻿using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;

namespace iOrder.Application.Users.Queries.Login
{
    public class LoginViewModel : IMapFrom<User>
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }
}
