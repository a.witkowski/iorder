﻿using MediatR;

namespace iOrder.Application.Users.Queries.Login
{
    public class LoginQuery : IRequest<string>
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }
}
