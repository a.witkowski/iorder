﻿using AutoMapper;
using iOrder.Application.Infrastructure.Tokens;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Users.Queries.Login
{
    public class LoginQueryHandler : IRequestHandler<LoginQuery, string>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<LoginQueryHandler> _logger;
        private readonly IConfiguration _configuration;

        public LoginQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<LoginQueryHandler> logger,
            IConfiguration configuration)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public async Task<string> Handle(LoginQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("LoginQueryHandler Start");

            if (string.IsNullOrWhiteSpace(request.Login) || string.IsNullOrWhiteSpace(request.Password))
            {
                _logger.LogWarning($"Login or Password is empty");
                throw new Exception("Поле пользователь или пароль пустое");
            }

            User user = await _context.Users
                .Include(x => x.UserRoles)
                .Include("UserRoles.Role")
                .SingleOrDefaultAsync(x => x.Login == request.Login && x.Password == request.Password);
            if (user == null)
            {
                _logger.LogWarning($"User {request.Login} not found");
                throw new Exception("Пользователь не найден или пароль не верен");
            }

            if (user.Deleted)
            {
                _logger.LogWarning($"User {request.Login} deleted");
                throw new Exception("Пользователь удален");
            }

            try
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, request.Login)
                };

                ClaimsIdentity claimsIdentity =
                    new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                        ClaimsIdentity.DefaultRoleClaimType);

                var token = _configuration.GetSection("JWTToken").Get<JWTToken>();
                var now = DateTime.UtcNow;

                var jwt = new JwtSecurityToken(
                        issuer: token.Issuer,
                        audience: token.Audience,
                        notBefore: now,
                        claims: claimsIdentity.Claims,
                        expires: now.Add(TimeSpan.FromMinutes(token.Expires)),
                        signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(token.Secret)), SecurityAlgorithms.HmacSha512));

                var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

                var response = new
                {
                    token = encodedJwt,
                    userId = user.Id,
                    roles = user.UserRoles.Select(x => x.Role.Name)
                };

                _logger.LogInformation("LoginQueryHandler End");

                return JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
