﻿using AutoMapper;
using iOrder.Application.Interfaces.Mapping;
using iOrder.Domain.Entities;
using System;

namespace iOrder.Application.Users.Queries.GetChildUsers
{
    public class ChildUserViewModel : IHaveCustomMapping
    {
        public Guid UserId { get; set; }

        public string Email { get; set; }

        public string Login { get; set; }

        public string Name { get; set; }

        public bool Deleted { get; set; }

        public DateTime Created { get; set; }

        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<User, ChildUserViewModel>()
                .ForMember(v => v.UserId, x => x.MapFrom(m => m.Id))
                ;
        }
    }
}
