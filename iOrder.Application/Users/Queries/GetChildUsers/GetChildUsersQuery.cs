﻿using MediatR;
using System;
using System.Collections.Generic;

namespace iOrder.Application.Users.Queries.GetChildUsers
{
    public class GetChildUsersQuery : IRequest<IEnumerable<ChildUserViewModel>>
    {
        public Guid UserId { get; set; }
    }
}
