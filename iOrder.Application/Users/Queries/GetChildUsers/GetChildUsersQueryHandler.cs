﻿using AutoMapper;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Users.Queries.GetChildUsers
{
    public class GetChildUsersQueryHandler : IRequestHandler<GetChildUsersQuery, IEnumerable<ChildUserViewModel>>
    {
        private readonly IOrderDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetChildUsersQueryHandler> _logger;

        public GetChildUsersQueryHandler(
            IOrderDbContext context,
            IMapper mapper,
            ILogger<GetChildUsersQueryHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<IEnumerable<ChildUserViewModel>> Handle(GetChildUsersQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("GetChildUsersQueryHandler Start");

            try
            {
                IList<User> users = await _context.Users
                    .Where(x => x.ParentId == request.UserId)
                    .ToListAsync(cancellationToken);

                var model = _mapper.Map<IEnumerable<ChildUserViewModel>>(users);

                _logger.LogInformation("GetChildUsersQueryHandler End");

                return model;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
