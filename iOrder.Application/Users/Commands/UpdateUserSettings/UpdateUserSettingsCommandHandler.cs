﻿using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Users.Commands.UpdateUserSettings
{
    public class UpdateUserSettingsCommandHandler : IRequestHandler<UpdateUserSettingsCommand, Unit>
    {
        private readonly IOrderDbContext _context;
        private readonly ILogger<UpdateUserSettingsCommandHandler> _logger;

        public UpdateUserSettingsCommandHandler(
            IOrderDbContext context,
            ILogger<UpdateUserSettingsCommandHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Unit> Handle(UpdateUserSettingsCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("UpdateUserSettingsCommandHandler Start");

            User user = await _context.Users
                .Include(x => x.UserSetting)
                .SingleOrDefaultAsync(x => x.Id == request.UserId);
            if (user == null)
            {
                _logger.LogInformation($"User {request.UserId} not found");
                throw new NotFoundException(nameof(User), request.UserId);
            }

            try
            {
                var currentData = DateTime.Now;

                if (request.UsePharmacy.HasValue)
                    user.UserSetting.UsePharmacy = request.UsePharmacy.Value;

                if (request.UseCosmetic.HasValue)
                    user.UserSetting.UseCosmetic = request.UseCosmetic.Value;

                if (request.UseDietarySupplement.HasValue)
                    user.UserSetting.UseDietarySupplement = request.UseDietarySupplement.Value;

                if (request.UseOptics.HasValue)
                    user.UserSetting.UseOptics = request.UseOptics.Value;

                if (request.UseMedicalProducts.HasValue)
                    user.UserSetting.UseMedicalProducts = request.UseMedicalProducts.Value;

                if (request.UseMedicalEquipment.HasValue)
                    user.UserSetting.UseMedicalEquipment = request.UseMedicalEquipment.Value;

                user.UserSetting.Updated = currentData;

                await _context.SaveChangesAsync(cancellationToken);

                _logger.LogInformation("UpdateUserSettingsCommandHandler End");

                return Unit.Value;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
