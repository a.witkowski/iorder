﻿using MediatR;
using System;

namespace iOrder.Application.Users.Commands.UpdateUserSettings
{
    public class UpdateUserSettingsCommand : IRequest
    {
        public Guid UserId { get; set; }

        public bool? UsePharmacy { get; set; }

        public bool? UseDietarySupplement { get; set; }

        public bool? UseOptics { get; set; }

        public bool? UseCosmetic { get; set; }

        public bool? UseMedicalEquipment { get; set; }

        public bool? UseMedicalProducts { get; set; }
    }
}
