﻿using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Users.Commands.UpdateChildUser
{
    public class UpdateChildUserCommandHandler : IRequestHandler<UpdateChildUserCommand, Unit>
    {
        private readonly IOrderDbContext _context;
        private readonly ILogger<UpdateChildUserCommandHandler> _logger;

        public UpdateChildUserCommandHandler(
            IOrderDbContext context,
            ILogger<UpdateChildUserCommandHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Unit> Handle(UpdateChildUserCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("UpdateChildUserCommandHandler Start");

            User user = await _context.Users.SingleOrDefaultAsync(x => x.Id == request.UserId && x.ParentId == request.ParentId);
            if (user == null)
            {
                _logger.LogInformation($"User {request.ParentId} not found");
                throw new NotFoundException(nameof(User), request.ParentId);
            }

            try
            {
                var currentData = DateTime.Now;

                user.Name = request.Name;
                user.Deleted = request.Deleted;
                user.Updated = currentData;

                await _context.SaveChangesAsync(cancellationToken);

                _logger.LogInformation("UpdateChildUserCommandHandler End");

                return Unit.Value;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
