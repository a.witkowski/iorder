﻿using MediatR;
using System;

namespace iOrder.Application.Users.Commands.UpdateChildUser
{
    public class UpdateChildUserCommand : IRequest
    {
        public Guid UserId { get; set; }

        public string Name { get; set; }

        public bool Deleted { get; set; }

        public Guid ParentId { get; set; }
    }
}
