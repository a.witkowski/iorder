﻿using MediatR;
using System;

namespace iOrder.Application.Users.Commands.CreateChildUser
{
    public class CreateChildUserCommand : IRequest<Guid>
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string Name { get; set; }

        public Guid ParentId { get; set; }
    }
}
