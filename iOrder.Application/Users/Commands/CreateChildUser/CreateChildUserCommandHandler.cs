﻿using iOrder.Application.Infrastructure.Exceptions;
using iOrder.Application.Interfaces;
using iOrder.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Users.Commands.CreateChildUser
{
    public class CreateChildUserCommandHandler : IRequestHandler<CreateChildUserCommand, Guid>
    {
        private readonly IOrderDbContext _context;
        private readonly ILogger<CreateChildUserCommandHandler> _logger;

        public CreateChildUserCommandHandler(
            IOrderDbContext context,
            ILogger<CreateChildUserCommandHandler> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<Guid> Handle(CreateChildUserCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("CreateChildUserCommandHandler Start");

            bool isUserExists = await _context.Users.AnyAsync(x => x.Login == request.Email || x.Email == request.Email);
            if (isUserExists)
            {
                _logger.LogInformation($"User {request.Email} already exists");
                throw new Exception("Пользователь уже существует");
            }

            User user = await _context.Users.FindAsync(request.ParentId);
            if (user == null)
            {
                _logger.LogInformation($"User {request.ParentId} not found");
                throw new NotFoundException(nameof(User), request.ParentId);
            }

            Role role = await _context.Roles.SingleOrDefaultAsync(x => x.Name.ToLower() == "user");
            if (role == null)
            {
                _logger.LogInformation($"Role 'user' not found");
                throw new Exception("Отсутствует роль пользователя");
            }

            try
            {
                var currentData = DateTime.Now;

                var newUser = new User()
                {
                    OrganizationId = user.OrganizationId,
                    Email = request.Email,
                    Login = request.Email,
                    Password = request.Password,
                    Name = request.Name,
                    ParentId = user.Id,
                    Created = currentData,
                    Updated = currentData
                };

                newUser.UserRoles.Add(new UserRole() { RoleId = role.Id });

                //IList<Point> points = await _context.Points.Where(x => x.OrganizationId == user.OrganizationId).ToListAsync();
                //if (points != null && points.Count > 0)
                //{
                //    for (int i = 0; i < points.Count; i++)
                //    {
                //        newUser.UserPoints.Add(new UserPoint() { PointId = points[i].Id });
                //    }
                //}

                newUser.UserSetting = new UserSetting { Created = currentData, Updated = currentData };

                _context.Users.Add(newUser);

                await _context.SaveChangesAsync(cancellationToken);

                _logger.LogInformation("CreateChildUserCommandHandler End");

                return newUser.Id;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
