﻿using iOrder.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace iOrder.Application.Interfaces
{
    public interface IOrderDbContext
    {
        DbSet<Basket> Baskets { get; set; }

        DbSet<Contract> Contracts { get; set; }

        DbSet<ContractType> ContractTypes { get; set; }

        DbSet<Factory> Factories { get; set; }

        DbSet<Manufacturer> Manufacturers { get; set; }

        DbSet<Order> Orders { get; set; }

        DbSet<OrderItem> OrderItems { get; set; }

        DbSet<OrderItemState> OrderItemStates { get; set; }

        DbSet<Organization> Organizations { get; set; }

        DbSet<OrganizationPriceSetting> OrganizationPriceSettings { get; set; }

        DbSet<OrganizationPromotion> OrganizationPromotions { get; set; }

        DbSet<OrganizationSetting> OrganizationSettings { get; set; }

        DbSet<OrganizationType> OrganizationTypes { get; set; }

        DbSet<Point> Points { get; set; }

        DbSet<Product> Products { get; set; }

        DbSet<ProductPrice> ProductPrices { get; set; }

        DbSet<ProductType> ProductTypes { get; set; }

        DbSet<Promotion> Promotions { get; set; }

        DbSet<Provision> Provisions { get; set; }

        DbSet<Role> Roles { get; set; }

        DbSet<User> Users { get; set; }

        DbSet<UserPoint> UserPoints { get; set; }

        DbSet<UserSetting> UserSettings { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
