﻿using iOrder.Application.Emails;
using iOrder.Application.Emails.Models;
using System.Threading.Tasks;

namespace iOrder.Application.Interfaces
{
    public interface IEmailServices
    {
        Task<string> GetTemplateAsync(EmailTemplate template);

        Task SendAsync(MessageDto message);
    }
}
