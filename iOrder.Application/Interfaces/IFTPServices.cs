﻿using System.Threading.Tasks;

namespace iOrder.Application.Interfaces
{
    public interface IFTPServices
    {
        Task UploadAsync(string fileName, byte[] file);
    }
}
