﻿using System;

namespace iOrder.Domain.Entities
{
    public class Provision
    {
        public Guid Id { get; set; }

        public Guid OrganizationId { get; set; }

        public Guid? PointId { get; set; }

        public Guid ManufacturerId { get; set; }

        public Guid? ProductId { get; set; }

        public double? Price { get; set; }

        public int? Quantity { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public DateTime Updated { get; set; }

        public DateTime Created { get; set; }

        public Organization Organization { get; set; }

        public Point Point { get; set; }

        public Manufacturer Manufacturer { get; set; }

        public Product Product { get; set; }
    }
}
