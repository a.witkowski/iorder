﻿using System;

namespace iOrder.Domain.Entities
{
    public class UserSetting
    {
        public Guid Id { get; set; }

        #region Settings

        public bool UsePharmacy { get; set; }

        public bool UseDietarySupplement { get; set; }

        public bool UseOptics { get; set; }

        public bool UseCosmetic { get; set; }

        public bool UseMedicalEquipment { get; set; }

        public bool UseMedicalProducts { get; set; }

        #endregion

        public DateTime Updated { get; set; }

        public DateTime Created { get; set; }

        public User User { get; set; }
    }
}
