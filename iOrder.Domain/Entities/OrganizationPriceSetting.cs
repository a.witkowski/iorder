﻿using System;

namespace iOrder.Domain.Entities
{
    public class OrganizationPriceSetting
    {
        public Guid Id { get; set; }

        public Guid OrganizationId { get; set; }

        public Guid? FactoryId { get; set; }

        public string Id1C { get; set; }

        public string Code { get; set; }

        public bool HideWholesale { get; set; }

        public bool HideRetail { get; set; }

        public bool HideBarter { get; set; }

        public bool Deleted { get; set; }

        public DateTime Updated { get; set; }

        public DateTime Created { get; set; }

        public Organization Organization { get; set; }

        public Factory Factory { get; set; }
    }
}
