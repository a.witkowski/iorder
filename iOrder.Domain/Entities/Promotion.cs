﻿using System;
using System.Collections.Generic;

namespace iOrder.Domain.Entities
{
    public class Promotion
    {
        public Promotion()
        {
            OrganizationPromotions = new HashSet<OrganizationPromotion>();
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Link { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public bool Deleted { get; set; }

        public DateTime Updated { get; set; }

        public DateTime Created { get; set; }

        public ICollection<OrganizationPromotion> OrganizationPromotions { get; private set; }
    }
}
