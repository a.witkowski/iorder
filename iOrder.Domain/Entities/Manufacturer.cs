﻿using System;
using System.Collections.Generic;

namespace iOrder.Domain.Entities
{
    public class Manufacturer
    {
        public Manufacturer()
        {
            Factories = new HashSet<Factory>();
            Provisions = new HashSet<Provision>();
        }

        public Guid Id { get; set; }

        public string Id1C { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public int? PriceGroup { get; set; }

        public bool HideWholesale { get; set; }

        public bool HideRetail { get; set; }

        public bool HideBarter { get; set; }

        public bool Deleted { get; set; }

        public DateTime Updated { get; set; }

        public DateTime Created { get; set; }

        public ICollection<Factory> Factories { get; private set; }

        public ICollection<Provision> Provisions { get; private set; }
    }
}
