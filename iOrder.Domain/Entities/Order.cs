﻿using System;
using System.Collections.Generic;

namespace iOrder.Domain.Entities
{
    public class Order
    {
        public Order()
        {
            OrderItems = new HashSet<OrderItem>();
        }

        public Guid Id { get; set; }

        public Guid PointId { get; set; }

        public Guid UserId { get; set; }

        public string Number { get; set; }

        public DateTime Created { get; set; }

        public User User { get; set; }

        public Point Point { get; set; }

        public ICollection<OrderItem> OrderItems { get; private set; }
    }
}
