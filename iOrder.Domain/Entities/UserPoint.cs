﻿using System;

namespace iOrder.Domain.Entities
{
    public class UserPoint
    {
        public Guid UserId { get; set; }

        public User User { get; set; }

        public Guid PointId { get; set; }

        public Point Point { get; set; }
    }
}
