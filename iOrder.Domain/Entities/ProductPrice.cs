﻿using System;
using System.Collections.Generic;

namespace iOrder.Domain.Entities
{
    public class ProductPrice
    {
        public Guid Id { get; set; }

        public Guid ProductId { get; set; }

        public string Code { get; set; }

        public double Price { get; set; }

        public double PricePartner { get; set; }

        public double PriceBarter { get; set; }

        public DateTime? Expiration { get; set; }

        public DateTime? ExpirationPartner { get; set; }

        public DateTime? ExpirationBarter { get; set; }

        public int Quantity { get; set; }

        public int QuantityPartner { get; set; }

        public int QuantityBarter { get; set; }

        public string Lot { get; set; }

        public string LotPartner { get; set; }

        public string LotBarter { get; set; }

        public bool IsExpectedArrival { get; set; }

        public bool IsPromo { get; set; }

        public DateTime Updated { get; set; }

        public DateTime Created { get; set; }

        public Product Product { get; set; }
    }
}
