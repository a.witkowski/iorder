﻿using System;

namespace iOrder.Domain.Entities
{
    public class OrderItem
    {
        public Guid Id { get; set; }

        public Guid OrderId { get; set; }

        public Guid ProductId { get; set; }

        public int? OrderItemStateId { get; set; }

        public string Lot { get; set; }

        public DateTime? Expiration { get; set; }

        public double Price { get; set; }

        public int Quantity { get; set; }

        public DateTime Updated { get; set; }

        public DateTime Created { get; set; }

        public Order Order { get; set; }

        public Product Product { get; set; }

        public OrderItemState OrderItemState { get; set; }
    }
}
