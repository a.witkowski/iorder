﻿using System;

namespace iOrder.Domain.Entities
{
    public class Contract
    {
        public Guid Id { get; set; }

        public Guid OrganizationId { get; set; }

        public int ContractTypeId { get; set; }

        public string Id1C { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public DateTime Start { get; set; }

        public DateTime? End { get; set; }

        public bool Deleted { get; set; }

        public DateTime Updated { get; set; }

        public DateTime Created { get; set; }

        public Organization Organization { get; set; }

        public ContractType ContractType { get; set; }
    }
}
