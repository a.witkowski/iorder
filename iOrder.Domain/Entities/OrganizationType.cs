﻿using System.Collections.Generic;

namespace iOrder.Domain.Entities
{
    public class OrganizationType
    {
        public OrganizationType()
        {
            Organizations = new HashSet<Organization>();
        }

        public int Id { get; set; }

        public string Type { get; set; }

        public bool Deleted { get; set; }

        public ICollection<Organization> Organizations { get; private set; }
    }
}
