﻿using System.Collections.Generic;

namespace iOrder.Domain.Entities
{
    public class ProductType
    {
        public ProductType()
        {
            Products = new HashSet<Product>();
        }

        public int Id { get; set; }

        public string Type { get; set; }

        public ICollection<Product> Products { get; private set; }
    }
}
