﻿using System;
using System.Collections.Generic;

namespace iOrder.Domain.Entities
{
    public class Organization
    {
        public Organization()
        {
            Users = new HashSet<User>();
            Points = new HashSet<Point>();
            Contracts = new HashSet<Contract>();
            Provisions = new HashSet<Provision>();
            OrganizationPriceSettings = new HashSet<OrganizationPriceSetting>();
            OrganizationPromotions = new HashSet<OrganizationPromotion>();
        }

        public Guid Id { get; set; }

        public int OrganizationTypeId { get; set; }

        public string Id1C { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public bool Deleted { get; set; }

        public DateTime Updated { get; set; }

        public DateTime Created { get; set; }

        public OrganizationType OrganizationType { get; set; }

        public OrganizationSetting OrganizationSetting { get; set; }

        public ICollection<User> Users { get; private set; }

        public ICollection<Point> Points { get; private set; }

        public ICollection<Contract> Contracts { get; private set; }

        public ICollection<Provision> Provisions { get; private set; }

        public ICollection<OrganizationPriceSetting> OrganizationPriceSettings { get; private set; }

        public ICollection<OrganizationPromotion> OrganizationPromotions { get; private set; }
    }
}
