﻿using System.Collections.Generic;

namespace iOrder.Domain.Entities
{
    public class OrderItemState
    {
        public OrderItemState()
        {
            OrderItems = new HashSet<OrderItem>();
        }

        public int Id { get; set; }

        public string State { get; set; }

        public bool Deleted { get; set; }

        public ICollection<OrderItem> OrderItems { get; private set; }
    }
}
