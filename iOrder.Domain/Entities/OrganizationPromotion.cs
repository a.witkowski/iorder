﻿using System;

namespace iOrder.Domain.Entities
{
    public class OrganizationPromotion
    {
        public Guid OrganizationId { get; set; }

        public Organization Organization { get; set; }

        public Guid PromotionId { get; set; }

        public Promotion Promotion { get; set; }
    }
}
