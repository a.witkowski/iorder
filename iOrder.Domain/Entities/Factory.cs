﻿using System;
using System.Collections.Generic;

namespace iOrder.Domain.Entities
{
    public class Factory
    {
        public Factory()
        {
            Products = new HashSet<Product>();
            OrganizationPriceSettings = new HashSet<OrganizationPriceSetting>();
        }

        public Guid Id { get; set; }

        public Guid? ManufacturerId { get; set; }

        public string Id1C { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Country { get; set; }

        public bool Deleted { get; set; }

        public DateTime Updated { get; set; }

        public DateTime Created { get; set; }

        public Manufacturer Manufacturer { get; set; }

        public ICollection<Product> Products { get; private set; }

        public ICollection<OrganizationPriceSetting> OrganizationPriceSettings { get; private set; }
    }
}
