﻿using System;
using System.Collections.Generic;

namespace iOrder.Domain.Entities
{
    public class Product
    {
        public Product()
        {
            ProductPrices = new HashSet<ProductPrice>();
            Provisions = new HashSet<Provision>();
            OrderItems = new HashSet<OrderItem>();
        }

        public Guid Id { get; set; }

        public Guid? FactoryId { get; set; }

        public int ProductTypeId { get; set; }

        public string Id1C { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Barcode { get; set; }

        public bool IsColdZone { get; set; }

        public bool IsPrescription { get; set; }

        public bool IsEnrollPrice { get; set; }

        public string Pack { get; set; }

        public bool Deleted { get; set; }

        public DateTime Updated { get; set; }

        public DateTime Created { get; set; }

        public ProductType ProductType { get; set; }

        public Factory Factory { get; set; }

        public ICollection<ProductPrice> ProductPrices { get; private set; }

        public ICollection<Provision> Provisions { get; private set; }

        public ICollection<OrderItem> OrderItems { get; private set; }

        public ICollection<Basket> Baskets { get; set; }
    }
}
