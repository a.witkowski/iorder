﻿using System;

namespace iOrder.Domain.Entities
{
    public class Basket
    {
        public Guid UserId { get; set; }

        public Guid PointId { get; set; }

        public Guid ProductId { get; set; }

        public string Lot { get; set; }

        public DateTime? Expiration { get; set; }

        public double Price { get; set; }

        public int Quantity { get; set; }

        public DateTime Created { get; set; }

        public User User { get; set; }

        public Point Point { get; set; }

        public Product Product { get; set; }
    }
}
