﻿using System;
using System.Collections.Generic;

namespace iOrder.Domain.Entities
{
    public class Point
    {
        public Point()
        {
            Orders = new HashSet<Order>();
            UserPoints = new HashSet<UserPoint>();
            Provisions = new HashSet<Provision>();
        }

        public Guid Id { get; set; }

        public Guid OrganizationId { get; set; }

        public string Id1C { get; set; }

        public string Code { get; set; }

        public string Address { get; set; }

        public string Emails { get; set; }

        public bool Deleted { get; set; }

        public DateTime Updated { get; set; }

        public DateTime Created { get; set; }

        public Organization Organization { get; set; }

        public ICollection<Order> Orders { get; private set; }

        public ICollection<UserPoint> UserPoints { get; set; }

        public ICollection<Provision> Provisions { get; set; }

        public ICollection<Basket> Baskets { get; set; }
    }
}
