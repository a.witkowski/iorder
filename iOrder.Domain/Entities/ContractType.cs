﻿using System.Collections.Generic;

namespace iOrder.Domain.Entities
{
    public class ContractType
    {
        public ContractType()
        {
            Contracts = new HashSet<Contract>();
        }

        public int Id { get; set; }

        public string Type { get; set; }

        public bool Deleted { get; set; }

        public ICollection<Contract> Contracts { get; private set; }
    }
}
