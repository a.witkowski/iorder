﻿using System;
using System.Collections.Generic;

namespace iOrder.Domain.Entities
{
    public class User
    {
        public User()
        {
            Orders = new HashSet<Order>();
            ChildUsers = new HashSet<User>();
            UserRoles = new HashSet<UserRole>();
            UserPoints = new HashSet<UserPoint>();
            Baskets = new HashSet<Basket>();
        }

        public Guid Id { get; set; }

        public Guid? OrganizationId { get; set; }

        public string Email { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public string Name { get; set; }

        public Guid? ParentId { get; set; }

        public bool Deleted { get; set; }

        public DateTime Updated { get; set; }

        public DateTime Created { get; set; }

        public Organization Organization { get; set; }

        public User ParentUser { get; set; }

        public UserSetting UserSetting { get; set; }

        public ICollection<Order> Orders { get; private set; }

        public ICollection<User> ChildUsers { get; set; }

        public ICollection<UserRole> UserRoles { get; set; }

        public ICollection<UserPoint> UserPoints { get; set; }

        public ICollection<Basket> Baskets { get; private set; }
    }
}
