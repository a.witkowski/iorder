﻿using iOrder.Application.Promotions.Commands.CreatePromotion;
using iOrder.Application.Promotions.Commands.DeletePromotion;
using iOrder.Application.Promotions.Commands.UpdatePromotion;
using iOrder.Application.Promotions.Queries.GetPromotions;
using iOrder.Application.Promotions.Queries.GetValidPromotions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace iOrder.Api.Controllers
{
    public class PromotionsController : BaseController
    {
        const string PROMOTIONS_FOLDER = "promotionfiles";
        string PROMOTIONS_SAVE_PATH = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, PROMOTIONS_FOLDER);

        [HttpGet]
        public async Task<ActionResult<IEnumerable<PromotionViewModel>>> Get()
        {
            return Ok(await Mediator.Send(new GetPromotionsQuery()));
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<IEnumerable<ValidPromotionViewModel>>> GetValid(Guid userId)
        {
            return Ok(await Mediator.Send(new GetValidPromotionsQuery { UserId = userId }));
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromForm] CreatePromotionCommand command)
        {
            IFormFile file = Request.Form.Files[0];
            if (file.Length <= 0)
                return BadRequest();

            if (!Directory.Exists(PROMOTIONS_SAVE_PATH))
                Directory.CreateDirectory(PROMOTIONS_SAVE_PATH);

            string fileName = $"{Guid.NewGuid()}{Path.GetExtension(file.FileName).ToLower()}";

            string fullPath = Path.Combine(PROMOTIONS_SAVE_PATH, fileName);

            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            if (string.IsNullOrWhiteSpace(command.FileName))
                command.FileName = file.FileName;
            command.Link = $"/{PROMOTIONS_FOLDER}/{fileName}";

            return Ok(await Mediator.Send(command));
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Update([FromBody] UpdatePromotionCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{promotionId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(Guid promotionId)
        {
            await Mediator.Send(new DeletePromotionCommand { PromotionId = promotionId });

            return NoContent();
        }
    }
}