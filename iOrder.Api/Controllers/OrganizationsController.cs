﻿using iOrder.Application.Organizations.Commands.UpdateOrganizationSettings;
using iOrder.Application.Organizations.Queries.GetContracts;
using iOrder.Application.Organizations.Queries.GetOrganizations;
using iOrder.Application.Organizations.Queries.GetOrganizationSettings;
using iOrder.Application.Organizations.Queries.GetOrganizationsSettings;
using iOrder.Application.Organizations.Queries.GetOrganizationUsers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace iOrder.Api.Controllers
{
    public class OrganizationsController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrganizationViewModel>>> GetOrganizations()
        {
            return Ok(await Mediator.Send(new GetOrganizationsQuery()));
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<OrganizationSettingViewModel>> GetOrganizationSettings(Guid userId)
        {
            return Ok(await Mediator.Send(new GetOrganizationSettingsQuery { UserId = userId }));
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrganizationsSettingViewModel>>> GetOrganizationsSettings()
        {
            return Ok(await Mediator.Send(new GetOrganizationsSettingsQuery()));
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<IEnumerable<ContractViewModel>>> GetContracts(Guid userId)
        {
            return Ok(await Mediator.Send(new GetContractsQuery { UserId = userId }));
        }

        [HttpGet("{organizationId}")]
        public async Task<ActionResult<IEnumerable<OrganizationUserViewModel>>> GetOrganizationUsers(Guid organizationId)
        {
            return Ok(await Mediator.Send(new GetOrganizationUsersQuery { OrganizationId = organizationId }));
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult> UpdateOrganizationSettings([FromBody] UpdateOrganizationSettingsCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }
    }
}