﻿using iOrder.Application.Points.Commands.CreateChildUserPoint;
using iOrder.Application.Points.Commands.DeleteChildUserPoint;
using iOrder.Application.Points.Queries.GetChildUsersPoints;
using iOrder.Application.Points.Queries.GetPoints;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace iOrder.Api.Controllers
{
    public class PointsController : BaseController
    {
        [HttpGet("{userId}")]
        public async Task<ActionResult<IEnumerable<PointViewModel>>> Get(Guid userId)
        {
            return Ok(await Mediator.Send(new GetPointsQuery { UserId = userId }));
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<IEnumerable<PointViewModel>>> GetChildUsersPoints(Guid userId)
        {
            return Ok(await Mediator.Send(new GetChildUsersPointsQuery { UserId = userId }));
        }

        [HttpPost]
        public async Task<IActionResult> CreateChildUserPoint([FromBody] CreateChildUserPointCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> DeleteChildUserPoint(Guid userId, Guid pointId)
        {
            await Mediator.Send(new DeleteChildUserPointCommand { UserId = userId, PointId = pointId });

            return NoContent();
        }
    }
}