﻿using iOrder.Application.Products.Queries.GetProductPrices;
using iOrder.Application.Products.Queries.GetProductsState;
using iOrder.Application.Products.Queries.GetProductTypes;
using iOrder.Application.Products.Queries.GetValidManufacturersProducts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace iOrder.Api.Controllers
{
    public class ProductsController : BaseController
    {
        [HttpGet("{userId}")]
        public async Task<ActionResult<IEnumerable<ProductPriceViewModel>>> GetProductPrices(Guid userId)
        {
            return Ok(await Mediator.Send(new GetProductPricesQuery { UserId = userId }));
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<IEnumerable<ProductTypeViewModel>>> GetProductTypes(Guid userId)
        {
            return Ok(await Mediator.Send(new GetProductTypesQuery { UserId = userId }));
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<IEnumerable<ProductStateViewModel>>> GetProductsState(Guid userId)
        {
            return Ok(await Mediator.Send(new GetProductsStateQuery { UserId = userId }));
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ValidManufacturersProductsViewModel>>> GetValidManufacturersProducts()
        {
            return Ok(await Mediator.Send(new GetValidManufacturersProductsQuery()));
        }
    }
}