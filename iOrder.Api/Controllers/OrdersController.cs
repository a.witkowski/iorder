﻿using iOrder.Application.Orders.Commands.CreateOrder;
using iOrder.Application.Orders.Queries.GetOrderItems;
using iOrder.Application.Orders.Queries.GetOrders;
using iOrder.Application.Orders.Queries.GetOrdersItemsQuantities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace iOrder.Api.Controllers
{
    public class OrdersController : BaseController
    {
        [HttpGet("{userId}")]
        public async Task<ActionResult<IEnumerable<OrderViewModel>>> Get(Guid userId)
        {
            return Ok(await Mediator.Send(new GetOrdersQuery { UserId = userId }));
        }

        [HttpPost]
        public async Task<ActionResult<Guid>> Create([FromBody] CreateOrderCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrderItemViewModel>>> GetOrderItems([FromQuery] GetOrderItemsQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrdersItemsQuantitiesModel>>> GetOrdersItemsQuantities([FromQuery] GetOrdersItemsQuantitiesQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
    }
}
