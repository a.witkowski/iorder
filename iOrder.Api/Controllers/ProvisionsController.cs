﻿using iOrder.Application.Provisions.Commands.CreateProvision;
using iOrder.Application.Provisions.Commands.DeleteProvision;
using iOrder.Application.Provisions.Queries.GetProvisions;
using iOrder.Application.Provisions.Queries.GetStatistics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace iOrder.Api.Controllers
{
    public class ProvisionsController : BaseController
    {
        [HttpGet("{userId}")]
        public async ValueTask<ActionResult<IEnumerable<ProvisionViewModel>>> Get(Guid userId)
        {
            return Ok(await Mediator.Send(new GetProvisionsQuery { UserId = userId }));
        }

        [HttpGet]
        public async ValueTask<ActionResult<IEnumerable<StatisticViewModel>>> GetStatistics([FromQuery] GetStatisticsQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpPost]
        public async ValueTask<ActionResult<Guid>> Create([FromBody] CreateProvisionCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{provisionId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async ValueTask<IActionResult> Delete(Guid provisionId)
        {
            await Mediator.Send(new DeleteProvisionCommand { ProvisionId = provisionId });

            return NoContent();
        }
    }
}