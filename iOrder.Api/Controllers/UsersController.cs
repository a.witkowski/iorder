﻿using iOrder.Application.Users.Commands.CreateChildUser;
using iOrder.Application.Users.Commands.UpdateChildUser;
using iOrder.Application.Users.Commands.UpdateUserSettings;
using iOrder.Application.Users.Queries.GetChildUsers;
using iOrder.Application.Users.Queries.GetUser;
using iOrder.Application.Users.Queries.Login;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace iOrder.Api.Controllers
{
    public class UsersController : BaseController
    {
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<string>> Login([FromBody] LoginQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<UserViewModel>> Get(Guid userId)
        {
            return Ok(await Mediator.Send(new GetUserQuery { UserId = userId }));
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<UserViewModel>> GetChildUsers(Guid userId)
        {
            return Ok(await Mediator.Send(new GetChildUsersQuery { UserId = userId }));
        }

        [HttpPost]
        public async Task<ActionResult<Guid>> CreateChildUser([FromBody] CreateChildUserCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> UpdateChildUser([FromBody] UpdateChildUserCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> UpdateUserSettings([FromBody] UpdateUserSettingsCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }
    }
}