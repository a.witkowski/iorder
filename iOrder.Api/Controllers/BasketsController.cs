﻿using iOrder.Application.Baskets.Commands.DeleteBasket;
using iOrder.Application.Baskets.Commands.UpsertBasket;
using iOrder.Application.Baskets.Queries.GetBasket;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace iOrder.Api.Controllers
{
    public class BasketsController : BaseController
    {
        [HttpGet("{userId}")]
        public async Task<ActionResult<IEnumerable<BasketViewModel>>> Get(Guid userId)
        {
            return Ok(await Mediator.Send(new GetBasketQuery { UserId = userId }));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Upsert([FromBody] UpsertBasketCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid userId)
        {
            await Mediator.Send(new DeleteBasketCommand { UserId = userId });

            return NoContent();
        }
    }
}