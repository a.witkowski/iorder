﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;

namespace iOrder.Api.Common
{
    internal class SwaggerFixArraysInXmlFilter : ISchemaFilter
    {
        public void Apply(OpenApiSchema schema, SchemaFilterContext context)
        {
            Type type = context.Type;

            if (!type.IsValueType && !type.Name.Equals("string", StringComparison.OrdinalIgnoreCase))
            {
                schema.Xml = new OpenApiXml { Name = type.Name };
                if (schema.Properties != null)
                {
                    foreach (var property in schema.Properties)
                    {
                        if (property.Value.Type == "array")
                        {
                            property.Value.Xml = new OpenApiXml
                            {
                                Name = $"{property.Key}",
                                Wrapped = true
                            };
                            property.Value.Items.Xml = new OpenApiXml
                            {
                                Name = $"{property.Value.Items.Type}",
                                Wrapped = true
                            };
                        }
                    }
                }
            }
        }
    }
}
