﻿using iOrder.Api.Common;
using iOrder.Application;
using iOrder.Application.Infrastructure.Tokens;
using iOrder.Data;
using iOrder.Infrastructure;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;

namespace iOrder.Api
{
    public class Startup
    {
        readonly string _policyName = "AllowSpecificOrigins";

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            services.Configure<Application.Infrastructure.FTP.Models.FTPSettings>(Configuration.GetSection("FTPSettings"));

            services.AddInfrastructure(Configuration);
            services.AddDatabase(Configuration);
            services.AddApplication();

            var token = Configuration.GetSection("JWTToken").Get<JWTToken>();
            var secret = System.Text.Encoding.ASCII.GetBytes(token.Secret);

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidIssuer = token.Issuer,

                        ValidateAudience = false,
                        ValidAudience = token.Audience,

                        ValidateLifetime = false,

                        ClockSkew = TimeSpan.Zero,

                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(secret)
                    };
                });

            services.AddHealthChecks()
                .AddDbContextCheck<OrderDbContext>();

            services.AddHsts(options =>
            {
                options.Preload = true;
                options.IncludeSubDomains = true;
                options.MaxAge = TimeSpan.FromDays(365);
            });

            services.AddCors(options =>
            {
                options.AddPolicy(_policyName,
                builder =>
                {
                    builder.AllowAnyOrigin();
                    builder.AllowAnyMethod();
                    builder.AllowAnyHeader();
                });
            });

            services.AddControllers()
                .AddNewtonsoftJson(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "iOrder API",
                    Version = "v1",
                    Description = "iOrder API",
                });

                s.SchemaFilter<SwaggerFixArraysInXmlFilter>();

                s.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory,
                    $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            app.UseDeveloperExceptionPage();
            app.UseDatabaseErrorPage();
            //}
            //else
            //{
            app.UseHsts();
            //}

            app.UseHealthChecks("/hc");

            app.UseCustomExceptionHandler();

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseSwagger();
            app.UseSwaggerUI(s =>
            {
                s.SwaggerEndpoint("/swagger/v1/swagger.json", "iOrder API");
                s.RoutePrefix = string.Empty;
            });

            app.UseRouting();

            app.UseCors(_policyName);

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
